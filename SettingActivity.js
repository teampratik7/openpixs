import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';

export default class ThirdActivity extends Component {

    constructor(props) {
 
      super(props);
    
      this.state={
        isLoading:true,
        dataList:[],
        userName:'',
        description:'',
        showEditables:false
      }

      YellowBox.ignoreWarnings([
       'Warning: componentWillMount is deprecated',
       'Warning: componentWillReceiveProps is deprecated',
     ]);
    
    }

    removeBack(){
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
  }


    componentDidMount(){
      BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);

      AsyncStorage.getItem('userGuid',(err,resultID)=>{
        AsyncStorage.getItem('userToken',(err,resultToken)=>{  
          const response = fetch(
            // ID Modify
            "https://qa.openpixs.com/opUserDashboardStats?id="+resultID, {
                method: 'GET',
                headers: {
                Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authentication' : resultToken,
                    // Modify
              },
              }) 
              .then((response) => { return  response.json() } ) 
              .catch((error) => console.warn("fetch error:", error))
              .then((response) => {
                console.log(JSON.stringify(response));
                this.setState({
                  dataList:response,
                  isLoading:false,
                });
              })
            });
          });
    }
  
  componentWillUnmount(){
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    }

  handleBackButton=()=>{
      this.props.navigation.navigate('Sixth');
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);

  }
    userUpdate(){
      this.setState({
        showEditables:false
      })
      AsyncStorage.getItem('userToken',(err,resultToken)=>{
       
        const response = fetch(
          "https://qa.openpixs.com/opUpdateUser", {
              method: 'POST',
              headers: {
              Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authentication' : resultToken,
            },
            body: JSON.stringify({
              deactivateAccount : 1,
              name:this.state.userName,
              description:this.state.description,
            }),
            }) 
            .then((response) => { return  response.json() } ) 
            .catch((error) => console.warn("fetch error:", error))
            .then((response) => {
              console.log(JSON.stringify(response));
              Toast.show(JSON.stringify(response),Toast.SHORT);
            });
          });
    }
 
       render()
       {
        const {state}=this.props.navigation
        
        BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
        
        if(this.state.isLoading){
          return(
            <View style={{flex:1,padding:8}}>
             <ActivityIndicator/>
            </View>
          )
        }
        else{
          return(
            
             <View >
               <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                  <View style={{flexDirection:'row',height:90,padding:16}}>
                    {this.state.dataList.profileImageUrl==null?
                      <Icon style={{marginRight :4}} name='account-circle' color='#6423a2' size={50}></Icon>
                      :
                      <Image style={{width:50,height:50,borderRadius:25,}} source={{uri:this.state.dataList.profileImageUrl}}></Image>
                    } 
                    <Text style={{marginLeft:16,fontSize:16,fontWeight:'bold',alignSelf:'center'}}>{this.state.dataList.name}</Text>
                  </View>
                  {this.state.showEditables?
                  <TouchableOpacity style={{alignSelf:'center'}} onPress={()=>this.userUpdate()}> 
                  <View style={{backgroundColor:'silver',height:40,width:40,borderRadius:150/2,alignSelf:'center',marginRight:8,padding:8}}>
                    <Icon style={{alignSelf:'center'}} name='save' color='#6423a2' size={25}></Icon>
                  </View>
                  </TouchableOpacity>
                  :
                  <TouchableOpacity style={{alignSelf:'center'}} onPress={()=>this.setState({showEditables:true})}> 
                  <View style={{backgroundColor:'silver',height:40,width:40,borderRadius:150/2,alignSelf:'center',marginRight:8,padding:8}}>
                    <Icon style={{alignSelf:'center'}} name='edit' color='#6423a2' size={25}></Icon>
                  </View>
                  </TouchableOpacity> 
                  }
              </View>
              {this.state.showEditables?
              <View >
                <TextInput
                      
                      style={{width:'100%',alignSelf:'center',height:40,fontSize:16,borderColor:'#EAEAEA',borderWidth:1,marginLeft:16}} 
                      placeholder="Enter New Name"
                      placeholderTextColor = 'grey'
                      selectionColor="#6423a2"
                      keyboardType="default"
                      defaultValue={this.state.dataList.name}
                      onChangeText={(text)=>this.setState({userName:text})}
                      />
                <TextInput 
                      style={{width:'100%',height:100,fontSize:16,
                      borderColor:'#EAEAEA',borderWidth:1,margin:8,padding:8}} 
                      placeholder="Enter New Description"
                      placeholderTextColor = 'grey'
                      selectionColor="#6423a2"
                      keyboardType="default"
                      defaultValue={this.state.dataList.description}
                      onChangeText={(text)=>this.setState({description:text})}
                      />
                </View> 
                :
                null
              }     
              <View style={{justifyContent:'center',alignItems:'flex-start',flexDirection:'column',marginLeft:16}}>
                <TouchableOpacity onPress={()=>this.userUpdate()}>
                <View style={{flexDirection:'row'}}>
                    <Icon name='account-box' color='#6423a2' size={30} ></Icon>
                    <Text style={{marginLeft:8,alignSelf:'center',fontWeight:'bold',width:150}}>Deactivate Account</Text>
                </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Thirteen',{id:'privacy',isRefreshing:true}),this.removeBack()}}>
                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:16}}>
                    <Icon  name='lock' color='#6423a2' size={30} ></Icon>
                    <Text style={{marginLeft:8,alignSelf:'center',fontWeight:'bold',width:150}}>Privacy</Text>
                </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Thirteen',{id:'faq',isRefreshing:true}),this.removeBack()}}>
                <View style={{flexDirection:'row',marginTop:16}}>
                    <Icon name='help' color='#6423a2' size={30}></Icon>
                    <Text style={{marginLeft:8,alignSelf:'center',fontWeight:'bold',width:150}}>FAQ's</Text>
                </View>
                </TouchableOpacity>
              </View>  
             </View>
          );
          }
        }
    }