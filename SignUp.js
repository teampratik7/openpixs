import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';

const styles = StyleSheet.create({
    container: {
      width:Dimensions.get('window').width,
      flexDirection:'row',
      flex:1,
      backgroundColor:'#000',
      justifyContent:'space-around'
      
    },
    navBar:{
      height: 55,
      alignSelf:'stretch',
      backgroundColor: '#fff',
      justifyContent:'space-around',
      alignContent:'center',
      alignItems:'center',
      flexDirection:'row',
      padding: 8,
      width:100,
      flex:1
    },
    navHead:{
      elevation: 4,
      flexDirection:'row',
      backgroundColor: '#000',
      flex:1,
      width:100
    },
    MainContainer :{
   
      flex:1,
      paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
      alignItems: 'center',
      justifyContent: 'center',
      },
  
      containerLogin : {
        backgroundColor:'#fff',
        flex: 1,
        alignItems:'center',
        justifyContent :'center'
      },
      signupTextCont : {
        justifyContent :'center',
        paddingVertical:16,
        flexDirection:'column'
      },
      signupText: {
        color:'black',
        fontSize:16,
        alignSelf:'center'
      },
      signupButton: {
        color:'#fff',
        fontSize:16,
        fontWeight:'500',
        alignSelf:'center'
      },
    inputBox: {
      width:300,
      backgroundColor:'rgba(255, 255,255,0.2)',
      borderRadius: 25,
      paddingHorizontal:16,
      fontSize:16,
      color:'grey',
      
    },
  
   
    button: {
      width:300,
      backgroundColor:'#6423a2',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:16
    },
    buttonVerify: {
      width:300,
      backgroundColor:'#6423a2',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:32
    },
    buttonSignUp: {
      width:300,
      backgroundColor:'#aa8cc5',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:16
    },
    buttonText: {
      fontSize:16,
      fontWeight:'500',
      color:'#ffffff',
      textAlign:'center'
    },
    containerForm : {
      flexGrow: 1,
      justifyContent:'center',
      alignItems: 'center'
    },
    imageThumbnail: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 50,
      width:50,
      marginTop:20,
    borderRadius:25
  
    }
  });
export default class Signup extends React.Component {
    constructor(props){
      super(props);
      this.state={
        isLoading:false,
        dataSource:null,
        userEmail:'',
        userPassword:'',
        name:''

      }
      
    }
  
    signIn(){
      
      this.setState({
        isLoading:true,
      })
      const response = fetch(
        "https://qa.openpixs.com/opSignup", {
            method: 'POST',
            headers: {
            Accept: 'application/json',
                'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: this.state.userEmail,
            password : this.state.userPassword,
            name : this.state.userName,
            isBrand : '0',
            isMobileUser: 'true'
          }),
          }) 
          .then((response) => { return  response.json() } ) 
          .catch((error) => console.warn("fetch error:", error))
          .then((response) => {
            console.log(JSON.stringify(response));
            if(response.status==400||response.status==500){
              Alert.alert(JSON.stringify(response.errormsg));
            }
            else{
              this.setState({
                isLoading:false
              })
              this.props.navigation.navigate('Eighth',{userEmail:this.state.userEmail,});
              Toast.show("OTP Sent!")

            }
          })
    }
  

    render() {
      const {navigate}=this.props.navigation;
      
      if(this.state.isLoading){
        return(
          <View style={{flex:1,padding:8}}>
           <ActivityIndicator/>
           <Text>Please wait...</Text>
          </View>
        )
      }
      else{
      return(
        <View style={styles.containerLogin}>
          
          <View style={styles.containerForm}>
            <Image source={{uri:'https://i.imgur.com/OgZY6Cw.png'}} style={{width:250,height:150}}></Image>
            <View style={{flexDirection:'row',borderBottomColor:'#6423a2',
            borderBottomWidth:2,margin:16}}>
              <Icon style={{alignSelf:'center'}} 
              name='account-box' color='#6423a2' size={30}></Icon>
              <TextInput style={styles.inputBox} 
                  placeholder="Name"
                  placeholderTextColor = 'grey'
                  selectionColor="#6423a2"
                  keyboardType="default"
                  onChangeText={(text)=>this.setState({userName:text})}
                  onSubmitEditing={()=> this.password.focus()}
                  />
                  
            </View>  
            <View style={{flexDirection:'row',borderBottomColor:'#6423a2',
            borderBottomWidth:2,margin:16}}>
              <Icon style={{alignSelf:'center'}} 
              name='email' color='#6423a2' size={30}></Icon>
              <TextInput style={styles.inputBox} 
                  placeholder="Email"
                  placeholderTextColor = 'grey'
                  selectionColor="#6423a2"
                  keyboardType="email-address"
                  onChangeText={(text)=>this.setState({userEmail:text})}
                  onSubmitEditing={()=> this.password.focus()}
                  />
            </View>  
            <View style={{flexDirection:'row',borderBottomColor:'#6423a2',borderBottomWidth:2,margin:16}}>
              <Icon style={{alignSelf:'center'}} 
              name='lock' color='#6423a2' size={30}></Icon>  
              <TextInput style={styles.inputBox}  
                  placeholder="Password"
                  secureTextEntry={true}
                  placeholderTextColor = "grey"
                  onChangeText={(text)=>this.setState({userPassword:text})}
                  ref={(input) => this.password = input}
                  />
            </View> 
            <TouchableOpacity style={styles.button} onPress={()=>this.signIn()}>
              <Text style={styles.buttonText}>Signup</Text>
            </TouchableOpacity>     
          </View>
          <View style={styles.signupTextCont}>
            <Text style={styles.signupText}>Already Have Account?</Text>
            <TouchableOpacity onPress={this.signup} onPress={()=>navigate('Fourth')}
            style={styles.buttonSignUp}>
            <Text style={styles.signupButton}> Login</Text>
            </TouchableOpacity>
          </View>
        </View>	
        )
    }
  }
  }