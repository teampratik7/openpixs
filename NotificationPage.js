import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';

export default class NotificationPage extends React.Component {

    constructor(props) {

      super(props);
      this.state={
        isLoading:true,
        dataList:[],
        userId:''
      }
    
      YellowBox.ignoreWarnings([
       'Warning: componentWillMount is deprecated',
       'Warning: componentWillReceiveProps is deprecated',
     ]);
    
    }
   
  componentDidMount(){
      BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);

      this._fetchData();
    }

  componentWillUnmount(){
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    }

  handleBackButton=()=>{
      this.props.navigation.navigate('Sixth');
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
  }

    _fetchData(){
      AsyncStorage.getItem('userToken',(err,result)=>{
        AsyncStorage.getItem('userGuid',(err,userId)=>{      
        const response = fetch(
          "https://qa.openpixs.com/opGetNotifications", {
              method: 'GET',
              headers: {
              Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authentication' : result,
                  // change authentication
            },
            }) 
            .then((response) => { return  response.json() } ) 
            .catch((error) => console.warn("fetch error:", error))
            .then((response) => {
              console.log(JSON.stringify(response));
              this.setState({
                dataList:response,
                isLoading:false,
                userId:userId
              });
            })
          });
        });
    }

    _updateView(id){
      this._fetchData();

      AsyncStorage.getItem('userToken',(err,resultToken)=>{
     
        const response = fetch(
          "https://qa.openpixs.com/opMarkNotificationRead?status=1&id="+id, {
              method: 'PUT',
              headers: {
              Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authentication' : resultToken,
            },
            }) 
            .then((response) => { return  response.json() } ) 
            .catch((error) => console.warn("fetch error:", error))
            .then((response) => {
              console.log(JSON.stringify(response));
             
            });
          });
    }

    _viewAll(){
      this._fetchData();
      AsyncStorage.getItem('userToken',(err,resultToken)=>{
     
        const response = fetch(
          "https://qa.openpixs.com/opMarkNotificationRead?status=1&all=1", {
              method: 'PUT',
              headers: {
              Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authentication' : resultToken,
            },
            }) 
            .then((response) => { return  response.json() } ) 
            .catch((error) => console.warn("fetch error:", error))
            .then((response) => {
              console.log(JSON.stringify(response));
              
            });
          });
    }

    render() {

      const {navigate}=this.props.navigation;
      if(this.state.isLoading){
        return(
          <View style={{flex:1,padding:8}}>
           <ActivityIndicator/>
          </View>
        )
      }
      else{
      return(
        <View>
          <View style={{alignItems:'center',flexDirection:'row',width:'100%',
                justifyContent:'space-around',marginTop:16}}>
              <TouchableOpacity onPress={()=>navigate('Sixth')}>
                <Icon style={{alignSelf:'center'}} 
                  name='arrow-back' color='#6423a2' size={30}></Icon>
              </TouchableOpacity>      
                <Text style={{color:"#6423a2",fontSize:20}}>NOTIFICATIONS</Text>
                <TouchableOpacity onPress={()=>this._viewAll()}>
                <Icon style={{alignSelf:'center'}} name='cancel' color='#6423a2' size={50}></Icon>
                </TouchableOpacity>
              </View>
          <FlatList
          data={this.state.dataList}
          renderItem={({item})=>
            <View style={{flexDirection:'column'}}>

              {item.type=='like'?
              <View style={{height:250,marginTop:4}}>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Tenth',
                      {imgSrc:item.imgGuid,isRefreshing:true}),this._updateView(item.id)}}>  
                <View style={{alignItems:'center',flexDirection:'row',width:'100%',
                      marginTop:16,marginLeft:16}}>
                  <Image style={{alignSelf:'center',height:30,width:30,borderRadius:15}} source={{uri:item.profileImg}} 
                      ></Image>
                  <Text style={{color:"#6423a2",fontSize:14,marginLeft:8,fontWeight:'bold'}}>{item.name+" Like your Photo"}</Text>
                </View>
                <View style={{flexDirection:'row-reverse',marginHorizontal:16}}>
                <Text>16s</Text>
                </View>
                <View style={{height:150,backgroundColor:'#EAEAEA',margin:16}}>
                  <Image style={{height:120,backgroundColor:'#fff',margin:16}} 
                  source={{uri:'https://d1n5ldzwzxqojp.cloudfront.net/'+this.state.userId+"/"+item.path}}></Image>
                </View>
                <View style={{width:'100%',backgroundColor:'#EAEAEA',height:1,marginTop:16}}></View>
                </TouchableOpacity>
              </View>
              :
                <View style={{height:50}}>
                  <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Eleventh',{userID:item.userGuid,isRefreshing:true}),this._updateView(item.id)}}> 
                  <View style={{alignItems:'center',flexDirection:'row',width:'100%',
                        marginTop:16,marginLeft:16}}>
                    <Image style={{alignSelf:'center',height:30,width:30,borderRadius:15}} source={{uri:item.profileImg}} 
                    ></Image>
                    <Text style={{color:"#6423a2",fontSize:14,marginLeft:8,fontWeight:'bold'}}>{item.name+" Followed You"}</Text>
                  </View>
                  <View style={{width:'100%',backgroundColor:'#EAEAEA',height:1,marginTop:16}}></View>           
                  </TouchableOpacity> 
              </View>
           }
           </View>
          }
          keyExtractor={(item,index)=>'list-item-${index}'}>
        </FlatList>	
      </View>
      )
      }
    }
  }