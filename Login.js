import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';

const styles = StyleSheet.create({
  container: {
    width:Dimensions.get('window').width,
    flexDirection:'row',
    flex:1,
    backgroundColor:'#000',
    justifyContent:'space-around'
    
  },
  navBar:{
    height: 55,
    alignSelf:'stretch',
    backgroundColor: '#fff',
    justifyContent:'space-around',
    alignContent:'center',
    alignItems:'center',
    flexDirection:'row',
    padding: 8,
    width:100,
    flex:1
  },
  navHead:{
    elevation: 4,
    flexDirection:'row',
    backgroundColor: '#000',
    flex:1,
    width:100
  },
  MainContainer :{
 
    flex:1,
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    alignItems: 'center',
    justifyContent: 'center',
    },

    containerLogin : {
      backgroundColor:'#fff',
      flex: 1,
      alignItems:'center',
      justifyContent :'center'
    },
    signupTextCont : {
      justifyContent :'center',
      paddingVertical:16,
      flexDirection:'column'
    },
    signupText: {
      color:'black',
      fontSize:16,
      alignSelf:'center'
    },
    signupButton: {
      color:'#fff',
      fontSize:16,
      fontWeight:'500',
      alignSelf:'center'
    },
  inputBox: {
    width:300,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'grey',
    
  },

 
  button: {
    width:300,
    backgroundColor:'#6423a2',
     borderRadius: 4,
      marginVertical: 10,
      paddingVertical: 13,
      marginTop:16
  },
  buttonVerify: {
    width:300,
    backgroundColor:'#6423a2',
     borderRadius: 4,
      marginVertical: 10,
      paddingVertical: 13,
      marginTop:32
  },
  buttonSignUp: {
    width:300,
    backgroundColor:'#aa8cc5',
     borderRadius: 4,
      marginVertical: 10,
      paddingVertical: 13,
      marginTop:16
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
  containerForm : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center'
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width:50,
    marginTop:20,
  borderRadius:25

  }
}); 
export default class Login extends React.Component {

    constructor(props){
      super(props);
      this.state={
        isLoading:false,
        dataSource:null,
        userEmail:'',
        userPassword:''
        
      }
      
    }

    removeBack(){
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
  }
    componentWillUnmount(){
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    }
  
    handleBackButton=()=>{
       if(this.state.canExit){
         BackHandler.exitApp();
       }
       this.setState({canExit:true});
         setTimeout(() => {
           this.setState({
              canExit:false
           })
         }, 200);
      return true;
    }
    

    componentDidMount(){
      BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);

      this.setState({})

      AsyncStorage.getItem('userEmail',(err,userEmail)=>{
          AsyncStorage.getItem('userPassword',(err,userPassword)=>{
            if(userEmail!=null){
              this.props.navigation.navigate('Sixth');
              this.removeBack()
              //this.signIn(userEmail,userPassword);
            }
            else{
              SplashScreen.hide();
            }
          })
                
      })
      
    }
    updateUI(){
      return(
          
        <View style={{flex:1,padding:8,alignItems:'center',alignSelf:'center'}}>
         <ActivityIndicator size="large"/>
        </View>
      )
    }

    signIn(userEmail,userPassword){
      this.updateUI();
      this.setState({
        isLoading:true,
      })
      Toast.show("Logging In....")
      const response = fetch(
        "https://qa.openpixs.com/opAuthenticateUser", {
            method: 'POST',
            headers: {
            Accept: 'application/json',
                'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: userEmail,
            password: userPassword,
          }),
          }) 
          .then((response) => { return  response.json() } ) 
          .catch((error) => console.warn("fetch error:", error))
          .then((response) => {
            console.log(JSON.stringify(response));
            if(response.status==400){
              Alert.alert(JSON.stringify(response.errormsg));
            }
            else{
              this.props.navigation.navigate('Sixth');
              AsyncStorage.setItem('userName',response.name);
              AsyncStorage.setItem('userGuid',response.guid);
              AsyncStorage.setItem('userToken',response.token);
              AsyncStorage.setItem('userEmail',userEmail);
              AsyncStorage.setItem('userPassword',userPassword);
              this.removeBack()
            }
          })
          if(this.password!=null){
          this.password.clear();
          this.userEmail.clear();
        }
          this.setState({
            userEmail:'',
            userPassword:'',
            isLoading:false,
          })
          
    }

    render() {
      const {navigate}=this.props.navigation;
      if(this.state.isLoading){
        return(
          <View style={{flex:1,padding:8,alignItems:'center',alignSelf:'center'}}>
           <ActivityIndicator/>
          </View>
        )
      }
      return(
        <View style={styles.containerLogin}>
          <View style={styles.containerForm}>
            
            <Image source={{uri:'https://i.imgur.com/OgZY6Cw.png'}} style={{width:250,height:150}}></Image>
            <View style={{flexDirection:'row',borderBottomColor:'#6423a2',
            borderBottomWidth:2,margin:16}}>
              <Icon style={{alignSelf:'center'}} 
              name='email' color='#6423a2' size={30}></Icon>
              <TextInput style={styles.inputBox} 
                  placeholder="Email"
                  placeholderTextColor = 'grey'
                  selectionColor="#6423a2"
                  keyboardType="email-address"
                  ref={(input)=>this.userEmail=input}
                  onChangeText={(text)=>this.setState({userEmail:text})}
                  
                  />
            </View>  
            <View style={{flexDirection:'row',borderBottomColor:'#6423a2',borderBottomWidth:2,margin:16}}>
              <Icon style={{alignSelf:'center'}} 
              name='lock' color='#6423a2' size={30}></Icon>  
              <TextInput style={styles.inputBox}  
                  placeholder="Password"
                  secureTextEntry={true}
                  placeholderTextColor = "grey"
                  ref={(input) => this.password = input}
                  onChangeText={(text)=>this.setState({userPassword:text})}
                  />
            </View> 
            {/* //onPress={()=>navigate('Sixth')}        */}
            <TouchableOpacity style={styles.button} onPress={()=>this.signIn(this.state.userEmail,
              this.state.userPassword)}>
              <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>     
          </View>
          <View style={styles.signupTextCont}>
            <Text style={styles.signupText}>Don't have an account yet?</Text>
            <TouchableOpacity onPress={()=>navigate('Fifth')} style={styles.buttonSignUp}>
            <Text style={styles.signupButton}> Signup</Text></TouchableOpacity>
          </View>
        </View>	
        )

        
    }

    
    
  }