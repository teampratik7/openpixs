import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';

export default class FollowersList extends React.Component {
    constructor(props) {

      super(props);
      this.state={
        isLoading:true,
        dataList:[],
        userId:'',
        myUserId:''
      }
    
      YellowBox.ignoreWarnings([
       'Warning: componentWillMount is deprecated',
       'Warning: componentWillReceiveProps is deprecated',
     ]);
    
    }
   
    componentDidMount(){
      BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
     // this.interval=setInterval(()=>this._fetchData,this.setState({time:Date.now()}),10000)  
  }
  
  componentWillUnmount(){
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    }

  handleBackButton=()=>{
    const {navigate}=this.props.navigation;
    const {state}=this.props.navigation;
    if(state.params.route=='userdashboard'){
      navigate('Eleventh',{route:'Search'});
    }else if(state.params.route=='Profile'){
      navigate('Second',{home:'Home'})
  }
  else{
    navigate('Second')
  }
  BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
}

    _fetchFollowers(){
      const {state}=this.props.navigation;
      AsyncStorage.getItem('userToken',(err,result)=>{
        AsyncStorage.getItem('userGuid',(err,userGuid)=>{      
        const response = fetch(
          "https://qa.openpixs.com​/opUserFollowerList?id="+state.params.userId+"&limit=100&offset=0", {
              method: 'GET',
              headers: {
              Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authentication' : result,
                  // change authentication
            },
            }) 
            .then((response) => { return  response.json() } ) 
            .catch((error) => console.warn("fetch error:", error))
            .then((response) => {
              console.log(JSON.stringify(response));
              this.setState({
                dataList:response,
                isLoading:false,
                myUserId:userGuid,
              });
            })
        });
      });
    }

    _fetchFollowing(){
      const {state}=this.props.navigation;
      AsyncStorage.getItem('userToken',(err,result)=>{
        AsyncStorage.getItem('userGuid',(err,userGuid)=>{          
        const response = fetch(
          "https://qa.openpixs.com​/opUserFollowingList?id="+state.params.userId+"&limit=100&offset=0", {
              method: 'GET',
              headers: {
              Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authentication' : result,
                  // change authentication
            },
            }) 
            .then((response) => { return  response.json() } ) 
            .catch((error) => console.warn("fetch error:", error))
            .then((response) => {
              console.log(JSON.stringify(response));
              this.setState({
                dataList:response,
                isLoading:false,
                myUserId:userGuid,
              });
            })
        });
      });
    }

    render() {
      const {state}=this.props.navigation; 
      if(!state.params.route==null){
        BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
      }
      const {navigate}=this.props.navigation;
      if(state.params.isFollow==1){
        this._fetchFollowers();
      }
      else{
        this._fetchFollowing();
      }
      return(
        <View>
          <FlatList
          data={this.state.dataList}
          renderItem={({item})=>
            <View>
              <View style={{flexDirection:'row',margin:4,alignContent:'center',
              justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('Eleventh',{userID:item.id,isRefreshing:true})}>
              {item.profileImage==null?
                <Icon style={{marginRight:16,marginTop:4}} name="account-circle" size={45} color="#6423a2"></Icon>
                :
                <Image style={{width:40,height:40,borderRadius:20,marginRight:16,marginTop:4}} source={{uri:item.profileImage}}></Image>
              }
              </TouchableOpacity>
                <Text style={{width:'60%',fontSize:16,fontWeight:'bold'}}>{item.name}</Text>
                {item.id==this.state.myUserId?
                <View style={{width:'20%'}}>
                </View>
                :
                <View style={{width:'20%'}}>
                {item.isfollowed==0?
                <Text>Non Following</Text>
                :
                <Text>Following</Text>
                }
                </View>
              }
              </View>
              <View style={{height:1,backgroundColor:'#EAEAEA',marginTop:4}}></View>
            </View>

          } 
          keyExtractor={(item,index)=>'list-item-${index}'}>
        </FlatList>	
        </View>
      )
  }
}