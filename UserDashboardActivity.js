import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';
import SearchUser from './SearchUser'
import { stat } from 'react-native-fs';

export default class UserDashboardActivity extends Component {

    constructor(props) {
   
      super(props);
      this.state={
        isLoading:true,
        dataList:[],
        imageList:[],
        userID:'',
        isRefreshing:false,
        isSecond:false,
      }
    
    }

    _followUnFollow(userId,action){
      AsyncStorage.getItem('userToken',(err,result)=>{

      const response = fetch(
        "https://qa.openpixs.com/opFollowUnfollowUser", {
            method: 'POST',
            headers: {
            Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authentication' : result,
          },
          body: JSON.stringify({
            userid:userId,
            action:action
          }),
          }) 
          .then((response) => { return  response.json() } ) 
          .catch((error) => console.warn("fetch error:", error))
          .then((response) => {
            console.log(JSON.stringify(response));
            if(response.status==400||response.status==500){
              Alert.alert(JSON.stringify(response.errormsg));
            }
            else{
              this._fetchData();
            }
          })
        })
    }
  
    _fetchData(){
      const {state}=this.props.navigation; 

        this.setState({
          userID:state.params.userID,
        })
        AsyncStorage.getItem('userToken',(err,resultToken)=>{
          const response = fetch(
            "https://qa.openpixs.com/opUserDashboardStats?id="+this.state.userID, {
                method: 'GET',
                headers: {
                Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authentication' : resultToken,
              },
              }) 
              .then((response) => { return  response.json() } ) 
              .catch((error) => console.warn("fetch error:", error))
              .then((response) => {
                console.log(JSON.stringify(response));
                this.setState({
                  dataList:response,
                  isLoading:false,
                  isRefreshing:false,
                  isSecond:true
                });
              })
        });
  
        AsyncStorage.getItem('userToken',(err,resultToken)=>{
          const responseImages = fetch(
           
            "https://qa.openpixs.com/opGetPortfolioImages?id="+this.state.userID+"&limit=50&offset=0", {
                method: 'GET',
                headers: {
                Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authentication' : resultToken,
                    
              },
              }) 
              .then((response) => { return  response.json() } ) 
              .catch((error) => console.warn("fetch error:", error))
              .then((response) => {
                console.log(JSON.stringify(response));
                this.setState({
                  imageList:response,
                  isLoading:false,
                 
                });
              })
            });
    }
  
    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
        this._fetchData();
       // this.interval=setInterval(()=>this._fetchData,this.setState({time:Date.now()}),10000)  
    }
    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
      }
  
    handleBackButton=()=>{
      const {navigate}=this.props.navigation;
      const {state}=this.props.navigation;
      if(state.params.route=='Search'){
        navigate('Seventh',{isRefreshing:true,home:'Home'});
      }
      else{
        navigate('Sixth')
      }
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    }
    
    removeBack(){
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
  }
     
       render()
       {
        const {state}=this.props.navigation;
        if(!state.params==null){
          BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
        }else{
          BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
        }
        
        if(this.state.isLoading){
          return(
            <View style={{flex:1,padding:8}}>
             <ActivityIndicator/>
            </View>
          )
        }
         
        
        if(state.params.isRefreshing){
          if(this.state.isSecond){
          this._fetchData();
          state.params.isRefreshing=false;
          }
        }

        if(this.state.isLoading){
          
          return(
            <View style={{flex:1,padding:8}}>
             <ActivityIndicator/>
            </View>
          )
        }
        else{
          
          return(
            
             <View style={{flex:1}}>
                <ScrollView style={{flex:1}}>
                  <Image style={{height:225,backgroundColor:'#9370DB'}}
                    source={{uri:this.state.dataList.coverImage1}}>
                  </Image>
                  <View style={{height:85,flexDirection:'row',backgroundColor:'#fff',padding:16,
                    justifyContent:'space-between'}}>
                    
                    <View style={{flexDirection:'row',alignContent:'center',alignItems:'center',
                      justifyContent:'center'}}>
                      {this.state.dataList.profileImageUrl==null?
                        <Icon style={{marginRight :4}} name='account-circle' color='#6423a2' size={50}></Icon>
                        :
                        <Image style={{width:40,height:40,borderRadius:20,}} source={{uri:this.state.dataList.profileImageUrl}}></Image>
                      }
                      <Text style={{marginLeft:8,alignSelf:'center',fontWeight:'bold'}}>{this.state.dataList.name}</Text>
                    </View>
                    {this.state.dataList.isFollow==0?  
                    <Button onPress={()=>this._followUnFollow(this.state.dataList.userid,"1")} style={{backgroundColor:'#6423a2',paddingLeft:12,paddingRight:12,height:34,alignSelf:'center'}}>
                      <Text style={{color:'white'}}>Follow</Text>
                    </Button>  
                      :
                    <Button onPress={()=>this._followUnFollow(this.state.dataList.userid,"0")} style={{backgroundColor:'#6423a2',paddingLeft:12,paddingRight:12,height:34,alignSelf:'center'}}>  
                      <Text style={{color:'white'}}>UnFollow</Text>
                    </Button>  
                    }    
                  </View>
                  <Text style={{marginLeft:8,alignSelf:'flex-start',fontWeight:'bold'}}>{this.state.dataList.description}</Text>
  
                  <View style={{height:85,flexDirection:'row',backgroundColor:'#fff',padding:16,justifyContent:'space-between'}}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Twelveth',
                    {userId:this.state.dataList.userid,isFollow:1,route:'userdashboard'}),this.removeBack()}}>
                    <View style={{flexDirection:'column'}}>
                      <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalfollowers}</Text>
                      <Text style={{fontSize:10,alignSelf:'center'}}>FOLLOWERS</Text>
                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Twelveth',
                    {userId:this.state.dataList.userid,isFollow:0,route:'userdashboard'}),this.removeBack()}}> 
                    <View style={{flexDirection:'column'}}>
                      <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalfollowing}</Text>
                      <Text style={{fontSize:10,alignSelf:'center'}}>FOLLOWING</Text>
                    </View>
                    </TouchableOpacity> 
                    <View style={{flexDirection:'column'}}>
                      <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalimages}</Text>
                      <Text style={{fontSize:10,alignSelf:'center'}}>IMAGE UPLOADED</Text>
                    </View>
                    <View style={{flexDirection:'column'}}>
                      <Text style={{fontSize:20,alignSelf:'center'}}>4</Text>
                      <Text style={{fontSize:10,alignSelf:'center'}}>USER LEVEL</Text>
                    </View>
                  </View>
                  <View >
                  <FlatList
                      data={this.state.imageList}
                      renderItem={({item})=>
                        <View style={{margin:8}}>
                        <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Tenth',
                        {imgSrc:item.id,isRefreshing:true,route:'userdashboard'}),this.removeBack()}}>
                          <Image style={{width:100,height:100,backgroundColor:'grey'}} source={{uri:
                            "https://d1n5ldzwzxqojp.cloudfront.net/"+this.state.userID+"/"+item.path}}></Image>
                        </TouchableOpacity>
                        </View>
                      }
                      numColumns={3}
                      keyExtractor={(item,index)=>index}>
                      </FlatList>
                  </View>
                  
                </ScrollView>
                
             </View>
          );
       }
      }
    }