import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';

export default class SecondActivity extends Component {

    constructor(props) {
   
      super(props);
      this.state={
        isLoading:true,
        dataList:[],
        imageList:[],
        userID:'',
        isRefreshing:false,
        uploadType:''
      }
    
    }
  
    _fetchData(){
  
      this.setState({
        isRefreshing:true,
      })
  
      AsyncStorage.getItem('userGuid',(err,resultID)=>{
        AsyncStorage.getItem('userToken',(err,resultToken)=>{
          const response = fetch(
            "https://qa.openpixs.com/opUserDashboardStats?id="+resultID, {
                method: 'GET',
                headers: {
                Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authentication' : resultToken,
              },
              }) 
              .then((response) => { return  response.json() } ) 
              .catch((error) => console.warn("fetch error:", error))
              .then((response) => {
                console.log(JSON.stringify(response));
                this.setState({
                  dataList:response,
                  isLoading:false,
                  userID:resultID,
                  isRefreshing:false
                });
              })
        });
      });
  
          
      AsyncStorage.getItem('userGuid',(err,resultID)=>{
        AsyncStorage.getItem('userToken',(err,resultToken)=>{
          const responseImages = fetch(
           
            "https://qa.openpixs.com/opGetPortfolioImages?id="+resultID+"&limit=50&offset=0", {
                method: 'GET',
                headers: {
                Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authentication' : resultToken,
                    
              },
              }) 
              .then((response) => { return  response.json() } ) 
              .catch((error) => console.warn("fetch error:", error))
              .then((response) => {
                console.log(JSON.stringify(response));
                this.setState({
                  imageList:response,
                  isLoading:false,
                 
                });
              })
            });
          });
    }
  
    _onRefresh=()=>{
      this._fetchData();
    }
  
    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);

        this._fetchData();
       // this.interval=setInterval(()=>this._fetchData,this.setState({time:Date.now()}),10000)  
    }
    
    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
      }
    removeBack(){
        BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    }
    handleBackButton=()=>{
        this.props.navigation.navigate('Sixth');
        BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    }
  
  uploadProfileImage(picType){
      const options = {
        quality: 1.0,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true,
        },
  };
    if(picType=='profile')
      this.setState({
        uploadType:'profile'
      })
  
      if(picType=='cover')
      this.setState({
          uploadType:'cover'
      })
      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
  
        if (response.didCancel) {
          console.log('User cancelled photo picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
         // let source = { uri: response.uri };
  
          // You can also display the image using data:
          // let source = { uri: 'data:image/jpeg;base64,' + response.data };
          // let source = response.data
          // Toast.show(source.toString)
           
          const RNFS=require('react-native-fs');
          RNFS.readFile(response.uri,"base64").then(data=>{
            this.uploadImageS3(data)
          })
          
        }
      });
  }
  
  uploadImageS3(base64Image){
  
    AsyncStorage.getItem('userToken',(err,resultToken)=>{
         
      const response = fetch(
        "https://qa.openpixs.com/opS3UploadImage", {
            method: 'POST',
            headers: {
            Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authentication' : resultToken,
          },
          body: JSON.stringify({
            uploadType:"public",
            base64String:base64Image
          }),
          }) 
          .then((response) => { return  response.json() } ) 
          .catch((error) => console.warn("fetch error:", error))
          .then((response) => {
            console.log(JSON.stringify(response));
            this.updateProfile(response.path)
          });
        });
  }
  
  updateProfile(path){
    if(this.state.uploadType=='profile'){
  
    AsyncStorage.getItem('userToken',(err,resultToken)=>{
      AsyncStorage.getItem('userGuid',(err,userGuid)=>{      
      const response = fetch(
        "https://qa.openpixs.com/opUpdateUser", {
            method: 'POST',
            headers: {
            Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authentication' : resultToken,
          },
          body: JSON.stringify({
            profile_image_url:'https://d1n5ldzwzxqojp.cloudfront.net/'+userGuid+"/"+path
          }),
          }) 
          .then((response) => { return  response.json() } ) 
          .catch((error) => console.warn("fetch error:", error))
          .then((response) => {
            console.log(JSON.stringify(response));
            Toast.show(JSON.stringify(response),Toast.SHORT);
          });
        });
      });
      }
  
      if(this.state.uploadType=='cover'){
        AsyncStorage.getItem('userToken',(err,resultToken)=>{
             
          const response = fetch(
            "https://qa.openpixs.com/opUpdateUser", {
                method: 'POST',
                headers: {
                Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authentication' : resultToken,
              },
              body: JSON.stringify({
                cover_image_url_1:path
              }),
              }) 
              .then((response) => { return  response.json() } ) 
              .catch((error) => console.warn("fetch error:", error))
              .then((response) => {
                console.log(JSON.stringify(response));
                Toast.show(JSON.stringify(response),Toast.SHORT);  
              });
            });
          }
  
  }
     
       render()
       {
        const {state}=this.props.navigation;
        if(!state.params==null){
        if(state.params.home=='Home'){
        BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
        }
      }else
      BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
         if(this.state.isLoading){
          
          return(
            <View style={{flex:1,padding:8}}>
             <ActivityIndicator/>
            </View>
          )
        }
        else{
          
          return(
             <View style={{flex:1}}>
                <ScrollView style={{flex:1}}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.isRefreshing}
                    onRefresh={this._onRefresh}
                  ></RefreshControl>} 
                >
                  <TouchableOpacity onPress={()=>{this.uploadProfileImage('cover')}}>
                  <Image style={{height:225,backgroundColor:'#9370DB'}}
                    source={{uri:this.state.dataList.coverImage1}}>
                  </Image>
                  </TouchableOpacity>
                  <View style={{height:85,flexDirection:'row',backgroundColor:'#fff',padding:16,
                    justifyContent:'space-between'}}>
                    
                    <View style={{flexDirection:'row',alignContent:'center',alignItems:'center',
                      justifyContent:'center'}}>
                      <TouchableOpacity onPress={()=>{this.uploadProfileImage('profile')}}>
                      {this.state.dataList.profileImageUrl==null?
                        <Icon style={{marginRight :4}} name='account-circle' color='#6423a2' size={50}></Icon>
                        :
                        <Image style={{width:40,height:40,borderRadius:20,}} source={{uri:this.state.dataList.profileImageUrl}}></Image>
                      }
                      </TouchableOpacity>
                      <Text style={{marginLeft:8,alignSelf:'center',fontWeight:'bold'}}>{this.state.dataList.name}</Text>
                      <Text style={{marginLeft:8,alignSelf:'flex-start',fontWeight:'bold'}}>{this.state.dataList.description}</Text>
                    </View>  
                  </View>
  
                  <View style={{height:85,flexDirection:'row',backgroundColor:'#fff',padding:16,justifyContent:'space-between'}}>
                      <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Twelveth',
                      {userId:this.state.dataList.userid,isFollow:1,route:'Profile'}),this.removeBack()}}>
                      <View style={{flexDirection:'column'}}>
                        <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalfollowers}</Text>
                        <Text style={{fontSize:10,alignSelf:'center'}}>FOLLOWERS</Text>
                      </View>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Twelveth',
                      {userId:this.state.dataList.userid,isFollow:0,route:'Profile'}),this.removeBack()}}> 
                      <View style={{flexDirection:'column'}}>
                        <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalfollowing}</Text>
                        <Text style={{fontSize:10,alignSelf:'center'}}>FOLLOWING</Text>
                      </View>
                      </TouchableOpacity> 
                      <View style={{flexDirection:'column'}}>
                        <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalimages}</Text>
                        <Text style={{fontSize:10,alignSelf:'center'}}>IMAGE UPLOADED</Text>
                      </View>
                      <View style={{flexDirection:'column'}}>
                        <Text style={{fontSize:20,alignSelf:'center'}}>4</Text>
                        <Text style={{fontSize:10,alignSelf:'center'}}>USER LEVEL</Text>
                      </View>
                    </View>
                    <View >
                    <FlatList
                        data={this.state.imageList}
                        renderItem={({item})=>
                          <View style={{margin:8}}>
                          <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Tenth',
                          {imgSrc:item.id,isRefreshing:true,route:'Profile'}),this.removeBack()}}>
                            <Image style={{width:100,height:100,backgroundColor:'grey'}} source={{uri:
                              "https://d1n5ldzwzxqojp.cloudfront.net/"+this.state.userID+"/"+item.path}}></Image>
                          </TouchableOpacity>
                          </View>
                        }
                        numColumns={3}
                        keyExtractor={(item,index)=>index}>
                        </FlatList>
                    </View>
                  
                </ScrollView>
                
             </View>
          );
       }
      }
    }