import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator,NavigationActions } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';

const styles = StyleSheet.create({
    container: {
      width:Dimensions.get('window').width,
      flexDirection:'row',
      flex:1,
      backgroundColor:'#000',
      justifyContent:'space-around'
      
    },
    navBar:{
      height: 55,
      alignSelf:'stretch',
      backgroundColor: '#fff',
      justifyContent:'space-around',
      alignContent:'center',
      alignItems:'center',
      flexDirection:'row',
      padding: 8,
      width:100,
      flex:1
    },
    navHead:{
      elevation: 4,
      flexDirection:'row',
      backgroundColor: '#000',
      flex:1,
      width:100
    },
    MainContainer :{
   
      flex:1,
      paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
      alignItems: 'center',
      justifyContent: 'center',
      },
  
      containerLogin : {
        backgroundColor:'#fff',
        flex: 1,
        alignItems:'center',
        justifyContent :'center'
      },
      signupTextCont : {
        justifyContent :'center',
        paddingVertical:16,
        flexDirection:'column'
      },
      signupText: {
        color:'black',
        fontSize:16,
        alignSelf:'center'
      },
      signupButton: {
        color:'#fff',
        fontSize:16,
        fontWeight:'500',
        alignSelf:'center'
      },
    inputBox: {
      width:300,
      backgroundColor:'rgba(255, 255,255,0.2)',
      borderRadius: 25,
      paddingHorizontal:16,
      fontSize:16,
      color:'grey',
      
    },
  
   
    button: {
      width:300,
      backgroundColor:'#6423a2',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:16
    },
    buttonVerify: {
      width:300,
      backgroundColor:'#6423a2',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:32
    },
    buttonSignUp: {
      width:300,
      backgroundColor:'#aa8cc5',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:16
    },
    buttonText: {
      fontSize:16,
      fontWeight:'500',
      color:'#ffffff',
      textAlign:'center'
    },
    containerForm : {
      flexGrow: 1,
      justifyContent:'center',
      alignItems: 'center'
    },
    imageThumbnail: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 50,
      width:50,
      marginTop:20,
    borderRadius:25
  
    }
  });
export default class SearchUser extends Component {
    constructor() {
      super();
      this.state = {
        dataSource: [],
        isLoading:true,
        searchText:"",
        searchType:'user',
        userGuid:'',
        isEmpty:true,
        isFirst:true
      };

      this.arrayHolder=[];
    }
  componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);

      this._fetchData();
    }

  componentWillUnmount(){
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    }

    removeBack(){
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
}

  handleBackButton=()=>{
    const {navigate}=this.props.navigation;
    navigate('Sixth');
    BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
  }

    _fetchData(){
      this.setState({
        isLoading:true,
      })
      
      const response = fetch("https://qa.openpixs.com/opSearch?cursor=initial&dboffset=0&limit=200&term="+this.state.searchText+"&type="+this.state.searchType, {
              method: 'GET',
              headers: {
              Accept: 'application/json',
                  'Content-Type': 'application/json',
                  
            },
            }) 
            .then((response) => { return  response.json() } ) 
            .catch((error) => console.warn("fetch error:", error))
            .then((response) => {
              console.log(JSON.stringify(response));
              if(response.data==[]){
                this.setState({
                  isEmpty:true
                })
              }
              else{
              this.setState({
                dataSource:response.data,
                isLoading:false,
                isEmpty:false,
              });
              this.arrayHolder=response.data;
            }
            })
            
    }
    // searchFilterFun=text=>{
    //       if(this.state.searchType=='user'){
    //       const newData=this.arrayHolder.filter(item=>{
    //       const itemData=`${item.name.toUpperCase()}`;
    //       const textData=text.toUpperCase();
    //       return itemData.indexOf(textData)>-1;
    //     });
    //     this.setState({dataSource:newData,searchText:text})
    //   }

    //     if(this.state.searchType=='label'){
    //       const newData=this.arrayHolder.filter(item=>{
    //       const itemData=`${item.tags.toUpperCase()}`;
    //       const textData=text.toUpperCase();
    //       return itemData.indexOf(textData)>-1;
    //     });
    //     this.setState({dataSource:newData,searchText:text})
    //   }

    //     if(this.state.searchType=='location'){
    //       const newData=this.arrayHolder.filter(item=>{
    //       const itemData=`${item.location.toUpperCase()}`;
    //       const textData=text.toUpperCase();
    //       return itemData.indexOf(textData)>-1;
    //     });
    //     this.setState({dataSource:newData,searchText:text})
    //   }

        
    // }

    updateSearch=(searchType)=>{
      this.setState({
        searchType:searchType,
        dataSource:[],
      })

      this.arrayHolder=[];
      
    }
    // renderHeader=()=>{
    //   const {state}=this.props.navigation;
    //   let search=this.state.searchText;
    //   if(state.params==null){
        
    //   }
    //   else{
    //     if(state.params.isRefreshing){
    //      this.searchFilterFun(state.params.search)
    //      state.params.isRefreshing=false;
    //     }
    //   }
    
    //   return(
    //     <View>
    //     <SearchBar
    //         placeholder="Search Here..."
    //         inputStyle={{backgroundColor:'white',height:30,borderColor:'white'}}
    //         containerStyle={{backgroundColor:'white',borderColor:'white'}}
    //         inputContainerStyle={{backgroundColor:'white',borderColor:'white'}}
    //         onChangeText={searchText=>this.searchFilterFun(searchText)}
    //         value={search}
    //         autoCorrect={false}
    //       >
    //       </SearchBar>
    //       </View>
          
    //   );
    // }

    // renderSeperator=()=>{
    //   return(
    //     <View
    //       style={{
    //         height:1,
    //         width:'86%',
    //         backgroundColor:'#fff',
    //         marginLeft:'14%',
    //       }}
    //     >

    //     </View>
    //   );
    // }
    render() {
      const {state}=this.props.navigation;
      if(state.params==null){
          BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
      }
      else{
        if(state.params.home=='Home')
          BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
        if(state.params.isRefreshing){
         this.setState({
           searchText:state.params.search
         })
         this._fetchData();
         state.params.isRefreshing=false;
        }
      }

      if(!this.state.isFirst){
      if(this.state.isLoading){
        return(
          <View style={{flex:1,padding:8}}>
           <ActivityIndicator/>
          </View>
        )
      }
      this.setState({
        isFirst:false
      })
    }
      else{
      return (
        <View style={{justifyContent: 'center',flex: 1,}}>
          
        <ScrollView>
          <View style={{flexDirection:'row'}}>
          <View style={{width:'30%',height:45,borderWidth:1,borderColor:'#EAEAEA'}}>
            <Picker selectedValue={this.state.searchType} onValueChange={this.updateSearch}>
              <Picker.Item label="Users" value="user"/>
              <Picker.Item label="Label" value="label"/>
              <Picker.Item label="Location" value="location"/>
            </Picker>
          </View>
            <TextInput style={{width:'70%',height:45,paddingHorizontal:16,borderWidth:1,
                                borderColor:'#EAEAEA'}} 
                placeholder="Enter Search here..."
                onChangeText={searchText=>this.setState({searchText:searchText})}
                placeholderTextColor = '#6423a2'
                selectionColor="#6423a2"
                keyboardType="default"
                />   
          </View>
          <View style={{width:'100%',flexDirection:'row',justifyContent:'center'}}>
          <Button style={{backgroundColor:'#6423a2',width:'90%',height:45,justifyContent:'center',
          marginTop:4,alignSelf:'center',elevation:10}} onPress={()=>this._fetchData()}>
            <Text style={{alignSelf:'center',color:'#fff',fontWeight:'bold'}}>SEARCH</Text> 
          </Button>
          </View>
          <View>
            {this.state.isEmpty?
            <Text style={{alignSelf:'center'}}>No Item Found for a Search!!</Text>
            :
            null
            }
          </View>
          {this.state.searchType=='user'?
          <FlatList
            data={this.state.dataSource}
            renderItem={({ item }) => (
              <View style={{ flex: 1, flexDirection: 'column', margin: 1,width:100,height:100,alignItems:'center'}}>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Eleventh',{userID:item.guid,
                  isRefreshing:true,route:'Search'}),this.removeBack()}}>
                 {item.profile_image_url==null?
                 <Icon style={styles.imageThumbnail} name="account-circle" size={50} />
                :

                <Image style={styles.imageThumbnail} source={{ uri: item.profile_image_url }} />
                }
                </TouchableOpacity>
              <Text style={{textAlign:'center',width:100,marginBottom:20,}}>{item.name}</Text>
              </View>
            )}
            //Setting the number of column
            numColumns={4}
            keyExtractor={(item) => item.profile_image_url}
            ItemSeparatorComponent={this.renderSeperator}
            ListHeaderComponent={this.renderHeader}
          />
          :
          <View style={{height:0}}></View>
          }
          {
            this.state.searchType=='label'?
          <FlatList
            data={this.state.dataSource}
            renderItem={({ item }) => (
              <View style={{ flex: 1, flexDirection: 'column', margin: 1,width:100,height:100,alignItems:'center'}}>
                 <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Tenth',
                      {imgSrc:item.imgguid,isRefreshing:true,route:'Search'}),this.removeBack()}}>
                  <Image style={{height:100,width:100,margin:4,backgroundColor:'grey'}} source={{ uri:
                  'https://d1n5ldzwzxqojp.cloudfront.net/'+item.userguid+'/'+item.path}} />
                  </TouchableOpacity>
              </View>
            )}
            //Setting the number of column
            numColumns={3}
            keyExtractor={(item) => item.path}
            ItemSeparatorComponent={this.renderSeperator}
            ListHeaderComponent={this.renderHeader}
          />
          :
          <View style={{height:0}}></View>
          }
          {
            this.state.searchType=='location'?
          <FlatList
            data={this.state.dataSource}
            renderItem={({ item }) => (
              <View style={{ flex: 1, flexDirection: 'column', margin: 1,width:100,height:100,alignItems:'center'}}>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Tenth',
                      {imgSrc:item.imgguid,isRefreshing:true,route:'Search'}),this.removeBack()}}>                  
                  <Image style={{height:100,width:100,margin:4}} source={{ uri:
                  'https://d1n5ldzwzxqojp.cloudfront.net/'+item.userguid+'/'+item.path}} />
                </TouchableOpacity>
              </View>
            )}
            //Setting the number of column
            numColumns={3}
            keyExtractor={(item) => item.profile_image_url}
            // ItemSeparatorComponent={this.renderSeperator}
            // ListHeaderComponent={this.renderHeader}
          />
          :
          <View style={{height:0}}></View>
          }
          </ScrollView>
        </View>
      );
      }
    }
  }