import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';

const styles = StyleSheet.create({
    container: {
      width:Dimensions.get('window').width,
      flexDirection:'row',
      flex:1,
      backgroundColor:'#000',
      justifyContent:'space-around'
      
    },
    navBar:{
      height: 55,
      alignSelf:'stretch',
      backgroundColor: '#fff',
      justifyContent:'space-around',
      alignContent:'center',
      alignItems:'center',
      flexDirection:'row',
      padding: 8,
      width:100,
      flex:1
    },
    navHead:{
      elevation: 4,
      flexDirection:'row',
      backgroundColor: '#000',
      flex:1,
      width:100
    },
    MainContainer :{
   
      flex:1,
      paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
      alignItems: 'center',
      justifyContent: 'center',
      },
  
      containerLogin : {
        backgroundColor:'#fff',
        flex: 1,
        alignItems:'center',
        justifyContent :'center'
      },
      signupTextCont : {
        justifyContent :'center',
        paddingVertical:16,
        flexDirection:'column'
      },
      signupText: {
        color:'black',
        fontSize:16,
        alignSelf:'center'
      },
      signupButton: {
        color:'#fff',
        fontSize:16,
        fontWeight:'500',
        alignSelf:'center'
      },
    inputBox: {
      width:300,
      backgroundColor:'rgba(255, 255,255,0.2)',
      borderRadius: 25,
      paddingHorizontal:16,
      fontSize:16,
      color:'grey',
      
    },
  
   
    button: {
      width:300,
      backgroundColor:'#6423a2',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:16
    },
    buttonVerify: {
      width:300,
      backgroundColor:'#6423a2',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:32
    },
    buttonSignUp: {
      width:300,
      backgroundColor:'#aa8cc5',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:16
    },
    buttonText: {
      fontSize:16,
      fontWeight:'500',
      color:'#ffffff',
      textAlign:'center'
    },
    containerForm : {
      flexGrow: 1,
      justifyContent:'center',
      alignItems: 'center'
    },
    imageThumbnail: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 50,
      width:50,
      marginTop:20,
    borderRadius:25
  
    }
  });
export default class OtpPage extends React.Component {

    constructor() {
      super();
      this.state = {
        userEmail:'',
        otpVal:0,
      };
    }

    componentDidMount(){
      const {state}=this.props.navigation; 
      this.setState({
        userEmail:state.params.userEmail,
      })
    }
  
    verifyOTP(otpVal){
      Toast.show(otpVal+this.state.userEmail)
      const response = fetch(
        "https://qa.openpixs.com/opValidateOTP", {
            method: 'POST',
            headers: {
            Accept: 'application/json',
                'Content-Type': 'application/json',
                
          },
          body: JSON.stringify({
            email : this.state.userEmail,
            otp : otpVal,
          }),
          }) 
          .then((response) => { return  response.json() } ) 
          .catch((error) => console.warn("fetch error:", error))
          .then((response) => {
            console.log(JSON.stringify(response));
              Toast.show(JSON.stringify(response),Toast.SHORT);
              this.props.navigation.navigate('Fourth')
          })

    }
    render() {

      const {navigate}=this.props.navigation;

      return(
        <View style={{height:400,flexDirection:'column'}}>
          
          <View style={styles.containerForm}>
              <Text style={{color:"#6423a2",fontSize:20}}>One Time Password(OTP)</Text>
              <View style={{borderBottomColor:'#6423a2',borderBottomWidth:2}}>
              <TextInput style={{width:150,backgroundColor:'rgba(255, 255,255,0.2)',
                borderRadius: 25,paddingHorizontal:16,fontSize:16,color:'grey',}}
                  selectionColor="#6423a2"
                  keyboardType="numeric"
                  onChangeText={(text)=>this.setState({otpVal:text})}
                  onSubmitEditing={()=> this.password.focus()}
                  /> 
               </View>     
            <TouchableOpacity style={styles.buttonVerify} onPress={()=>this.verifyOTP(
              this.state.otpVal)}>
              <Text style={styles.buttonText}>Verify</Text>
            </TouchableOpacity>     
          </View>
        </View>	
        )
    }
  }