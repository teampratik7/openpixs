import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';
import styles from './styles'

export default class ViewImage extends React.Component {

    constructor() {
      super();
      this.state = {
        isSecond:false,
        dataList:{},
        myGuid:'',
        imgLocation:'',
        imgTags:'',
        imgDescription:''
      };
    }

    _fetchData(imgId){
      AsyncStorage.getItem('userToken',(err,result)=>{
        AsyncStorage.getItem('userGuid',(err,userGuid)=>{
        const response = fetch(
          "https://qa.openpixs.com/opGetImageDetails?imgGuid="+imgId, {
              method: 'GET',
              headers: {
              Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authentication' : result,
                  
            },
            }) 
            .then((response) => { return  response.json() } ) 
            .catch((error) => console.warn("fetch error:", error))
            .then((response) => {
              console.log(JSON.stringify(response));
              this.setState({
                dataList:response,
                isSecond:true,
                myGuid:userGuid,
              });
            })
          });
        });
    }

    _onLikeImage(imageId,action){
      AsyncStorage.getItem('userToken',(err,resultToken)=>{
         
        const response = fetch(
          "https://qa.openpixs.com/opImageLikeUnlike", {
              method: 'POST',
              headers: {
              Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authentication' : resultToken,
            },
            body: JSON.stringify({
              imgGuid:imageId,
              action:action,
            }),
            }) 
            .then((response) => { return  response.json() } ) 
            .catch((error) => console.warn("fetch error:", error))
            .then((response) => {
              console.log(JSON.stringify(response));
              this._fetchData(imageId);
            });
          });
    }

    updateImageInfo(imageId){

        AsyncStorage.getItem('userToken',(err,resultToken)=>{
         
            const response = fetch(
              "https://qa.openpixs.com/opUpdateImageData", {
                  method: 'POST',
                  headers: {
                  Accept: 'application/json',
                      'Content-Type': 'application/json',
                      'Authentication' : resultToken,
                },
                body: JSON.stringify({
                    guid: imageId,
                    location: this.state.imgLocation,
                    tags:this.state.imgTags,
                    description: this.state.imgDescription
                }),
                }) 
                .then((response) => { return  response.json() } ) 
                .catch((error) => console.warn("fetch error:", error))
                .then((response) => {
                  console.log(JSON.stringify(response));
                  Toast.show("Saved");
                  this._fetchData(imageId);
                });
              });
    }

    handleBackButton=()=>{

      const {navigate}=this.props.navigation;
      const {state}=this.props.navigation;
      if(state.params.route=='Search'){
        navigate('Seventh',{home:'Home'});
      }else if(state.params.route=='Profile'){
        navigate('Second',{home:'Home'})
      }
      else if(state.params.route=='userdashboard'){
        navigate('Eleventh',{home:'Home'})
      }
      else{
        navigate('Sixth')
      }
      BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    }

      render() {
        
        const {state}=this.props.navigation;
        if(!state.params==null){
          BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
        }else{
          BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
        }
        if(state.params.isRefreshing){
          this._fetchData(state.params.imgSrc);
          state.params.isRefreshing=false;
          
        } 
        const {navigate}=this.props.navigation;
          return(
            <View style={{flex:1,flexDirection:'column'}}>
              <ScrollView>
              <View style={{height:55, flexDirection:'row',padding:8}}>
              {/* <TouchableOpacity onPress={()=>this.props.navigation.goBack(state.params.goBackKey)}>
                  <Icon style={{alignSelf:'center'}} name="arrow-back" size={30}></Icon>
                </TouchableOpacity> */}
                <View style={{flexDirection:'row',justifyContent:'center',width:'100%'}}>
                <TouchableOpacity style={{alignSelf:'center'}} onPress={()=>navigate('Sixth') }>
                  <Image source={{uri:'https://i.imgur.com/OgZY6Cw.png',width:150,height:40}}></Image>
                </TouchableOpacity> 
                </View> 
              </View>
              <View>
              <Image style={{height:350,margin:4}}
               source={{uri:"https://d1n5ldzwzxqojp.cloudfront.net/"+this.state.dataList.userguid+"/"+this.state.dataList.path}}>
               </Image>
              <View style={{padding:16,flexDirection:'row',justifyContent:'space-between',width:'70%',alignSelf:'center'}}>
                <View style={{justifyContent:'center'}}>
                <Image style={{width:30,height:30,alignSelf:'center'}} source={{uri:'https://i.imgur.com/5TTsKOj.png'}}></Image>
                <Text>{this.state.dataList.imageViewCount+" Views"}</Text>
                </View>
                <View>
                {this.state.dataList.isLiked==0?
                <TouchableOpacity onPress={()=>this._onLikeImage(state.params.imgSrc,"1")}>
                  <Image  style={{width:25,height:25}} source={{uri:"https://i.imgur.com/3Ak4Qch.png"}}></Image>
                </TouchableOpacity>
                  :
                <TouchableOpacity onPress={()=>this._onLikeImage(state.params.imgSrc,"0")}>
                  <Image style={{width:25,height:25}} source={{uri:"https://i.imgur.com/4ermCMf.png"}}></Image>
                </TouchableOpacity>
                }
                <Text>{this.state.dataList.likescount+" Likes"}</Text>
                </View>
              </View>
                {this.state.dataList.userguid==this.state.myGuid?
                <View>
                  <View style={{elevation:4,borderColor:'#EAEAEA',borderWidth:1,margin:2,height:150,justifyContent:'center'}}>
                    <Text style={{fontSize:18,marginLeft:4,fontWeight:'bold',alignSelf:'center'}}>DESCRIPTION:</Text>
                    <TextInput
                        style={{width:'100%',alignSelf:'center',height:100,fontSize:16,marginLeft:16}} 
                        placeholder="Enter Image Description"
                        placeholderTextColor = 'grey'
                        selectionColor="#6423a2"
                        keyboardType="default"
                        defaultValue={this.state.dataList.description}
                        onChangeText={(text)=>this.setState({imgDescription:text})}
                        />
                  </View>
                  <View style={{elevation:4,margin:2,height:120,justifyContent:'center'}}>      
                    <Text style={{fontSize:18,marginLeft:4,fontWeight:'bold',alignSelf:'center'}}>TAGS:</Text>    
                    <TextInput
                        
                        style={{width:'100%',alignSelf:'center',height:70,fontSize:16,marginLeft:16}} 
                        placeholder="Enter Tags"
                        placeholderTextColor = 'grey'
                        selectionColor="#6423a2"
                        keyboardType="default"
                        defaultValue={this.state.dataList.tags}
                        onChangeText={(text)=>this.setState({imgTags:text})}
                        /> 
                  </View>
                  <View style={{elevation:4,margin:2,height:120,justifyContent:'center'}}>
                    <Text style={{fontSize:18,marginLeft:4,fontWeight:'bold',alignSelf:'center'}}>LOCATION:</Text>   
                    <TextInput  
                        style={{width:'100%',alignSelf:'center',height:70,fontSize:16,marginLeft:16}} 
                        placeholder="Enter Location"
                        placeholderTextColor = 'grey'
                        selectionColor="#6423a2"
                        keyboardType="default"
                        defaultValue={this.state.dataList.location}
                        onChangeText={(text)=>this.setState({imgLocation:text})}
                        />
                  </View>      
                    <Button style={{width:'100%',justifyContent:'center',alignSelf:'center',backgroundColor:'#6423a2'}} onPress={()=>this.updateImageInfo(state.params.imgSrc)}>
                        <Text style={{alignSelf:'center',color:'#fff'}}>SAVE</Text>
                    </Button>               
                </View>
                :
                <View>
                    <View style={{elevation:4,borderColor:'#EAEAEA',borderWidth:1,margin:2,height:150,justifyContent:'center'}}>
                    <Text style={{fontSize:18,marginLeft:4,fontWeight:'bold',alignSelf:'center'}}>DESCRIPTION:</Text>                 
                    <Text style={{margin:8}}>{this.state.dataList.description}</Text>
                    </View>

                    <View style={{elevation:4,borderColor:'#EAEAEA',borderWidth:1,margin:2,height:110,justifyContent:'center'}}>
                    <Text style={{fontSize:18,marginLeft:4,fontWeight:'bold',alignSelf:'center'}}>TAGS:</Text>
                    <Text style={{margin:8}}>{this.state.dataList.tags}</Text>
                    </View>

                    <View style={{elevation:4,borderColor:'#EAEAEA',borderWidth:1,margin:2,height:110,justifyContent:'center'}}>
                    <Text style={{fontSize:18,marginLeft:4,fontWeight:'bold',alignSelf:'center'}}>LOCATION:</Text>
                    <Text style={{margin:8}}>{this.state.dataList.location}</Text>
                    </View>
                </View>
            }
            </View>
            </ScrollView>
            </View>
          )
        
      }
    }