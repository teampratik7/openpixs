import React, { Component } from 'react';
 
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox ,
  TextInput,FlatList,Alert,ActivityIndicator,ImageBackground,AsyncStorage,CameraRoll,
BackHandler,Dimensions,RefreshControl,Picker,Modal} from 'react-native'; 

import Toast from 'react-native-simple-toast';  
import { createDrawerNavigator,createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation'
import { Container, Button } from 'native-base';
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SearchBar } from 'react-native-elements';
import { ScrollView, WebView } from 'react-native-gesture-handler';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import ImagePicker from 'react-native-image-picker';
import SecondActivity from './SecondActivity'
import UserDashboardActivity from './UserDashboardActivity'
import ThirdActivity from './SettingActivity'
import PrivacyFaqPage from './PrivacyFaqPage'
import ViewImage from './ViewImage'
import SearchUser from './SearchUser'
import FollowersList from './FollowersList'
import NotificationPage from './NotificationPage'
import Login from './Login'
import Signup from './SignUp'
import OtpPage from './OTPpage'

import ReadMore from 'react-native-read-more-text';

class HamburgerIcon extends Component {

  toggleDrawer=()=>{

    console.log(this.props.navigationProps);
    
    this.props.navigationProps.toggleDrawer();

  }
 
  render() {
    const {navigate}=this.props.navigationProps;
    return (
 
      <View style={styles.container}>
        <View style={styles.navHead}>
          <View style={styles.navBar}>
            <TouchableOpacity onPress={this.toggleDrawer.bind(this) }>
              <Icon style={{marginRight :4}} name='menu' color='#6423a2' size={40}></Icon>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{navigate('Sixth'),BackHandler.removeEventListener
            ('hardwareBackPress',this.handleBackButton); }}>
            <Image source={{uri:'https://i.imgur.com/OgZY6Cw.png',width:150,height:40}}
              style={{marginLeft:40,marginRight:40}}></Image>
            </TouchableOpacity>  
            <TouchableOpacity onPress={()=>{navigate('Seventh'),BackHandler.removeEventListener
            ('hardwareBackPress',this.handleBackButton); } }>
              <Icon  name='search' color='#6423a2' size={30}></Icon>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{navigate('Ninth'),BackHandler.removeEventListener
            ('hardwareBackPress',this.handleBackButton); } }>
              <Icon  style={{marginLeft:8,alignSelf:'center'}} name='notifications' color='#6423a2' size={30}></Icon>
            </TouchableOpacity>  
          </View>
        </View>
      </View>
    
    );
  }
}

//Styles
const styles = StyleSheet.create({
  container: {
    width:Dimensions.get('window').width,
    flexDirection:'row',
    flex:1,
    backgroundColor:'#000',
    justifyContent:'space-around'
    
  },
  navBar:{
    height: 55,
    alignSelf:'stretch',
    backgroundColor: '#fff',
    justifyContent:'space-around',
    alignContent:'center',
    alignItems:'center',
    flexDirection:'row',
    padding: 8,
    width:100,
    flex:1
  },
  navHead:{
    elevation: 4,
    flexDirection:'row',
    backgroundColor: '#000',
    flex:1,
    width:100
  },
  MainContainer :{
 
    flex:1,
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    alignItems: 'center',
    justifyContent: 'center',
    },

    containerLogin : {
      backgroundColor:'#fff',
      flex: 1,
      alignItems:'center',
      justifyContent :'center'
    },
    signupTextCont : {
      justifyContent :'center',
      paddingVertical:16,
      flexDirection:'column'
    },
    signupText: {
      color:'black',
      fontSize:16,
      alignSelf:'center'
    },
    signupButton: {
      color:'#fff',
      fontSize:16,
      fontWeight:'500',
      alignSelf:'center'
    },
  inputBox: {
    width:300,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'grey',
    
  },

 
  button: {
    width:300,
    backgroundColor:'#6423a2',
     borderRadius: 4,
      marginVertical: 10,
      paddingVertical: 13,
      marginTop:16
  },
  buttonVerify: {
    width:300,
    backgroundColor:'#6423a2',
     borderRadius: 4,
      marginVertical: 10,
      paddingVertical: 13,
      marginTop:32
  },
  buttonSignUp: {
    width:300,
    backgroundColor:'#aa8cc5',
     borderRadius: 4,
      marginVertical: 10,
      paddingVertical: 13,
      marginTop:16
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
  containerForm : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center'
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
	width:50,
	marginTop:20,
  borderRadius:25

  }
});

class MainActivity extends Component {

	componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
    SplashScreen.hide();    
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
  }

  handleBackButton=()=>{
     if(this.state.canExit){
       BackHandler.exitApp();
     }
     this.setState({canExit:true});
       setTimeout(() => {
         this.setState({
            canExit:false
         })
       }, 200);
    return true;
  }

  constructor(props) {
 
    super(props);
    this.state={
      canExit:false,
      isLoading:true,
      dataList:[],
      statusTime:'',
      tagValue:'1',
      isRefreshing:false,
      showImage:true,
      isFirst:true,
      limit:10,
      offset:0  ,
      isRefreshingView:false
    }
  
  }

  setTagsVisible(tags){
      this.props.navigation.navigate('Fourteen',{tags:tags,isRefreshing:true})
  }
 

  fetchData(){
    AsyncStorage.getItem('userToken',(err,result)=>{
      const response = fetch(
        "https://qa.openpixs.com/opFeeds?limit="+this.state.limit+"&offset="+this.state.offset, {
            method: 'GET',
            headers: {
            Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authentication' : result,
                
          },
          }) 
          .then((response) => { return  response.json() } ) 
          .catch((error) => console.warn("fetch error:", error))
          .then((response) => {
            console.log(JSON.stringify(response));
            if(response==null) 
            {
            }
            else{
            this.setState({
              isLoading:false,
              isRefreshing:false,
              isFirst:false,
              dataList:this.state.dataList.concat(response),
              limit:this.state.limit+10,
              offset:this.state.offset+10,
              isRefreshingView:false
            });
            
          } 
          })
        });
        return;
  }


  //LAZY LOADING
  _onFetchNext(){
    if(!this.state.isRefreshing){
      if(!this.state.isRefreshingView){
        this.fetchData();
      }
    }
  }

  fetchRefresh(){
    AsyncStorage.getItem('userToken',(err,result)=>{
      const response = fetch(
        "https://qa.openpixs.com/opFeeds?limit=10&offset=0", {
            method: 'GET',
            headers: {
            Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authentication' : result,
                
          },
          }) 
          .then((response) => { return  response.json() } ) 
          .catch((error) => console.warn("fetch error:", error))
          .then((response) => {
            console.log(JSON.stringify(response));
            
            this.setState({
              dataList:response,
              isLoading:false,
              isRefreshingView:false
            });
          })
        });
        return;
  }

  _onRefresh(){

    if(!this.state.isRefreshing){
      Toast.show("hi")
      
        this.setState({
          limit:10,
          offset:0,
          isRefreshingView:true,
          dataList:[]
        })
        this.fetchRefresh();
    }
    
  }

  _onLikeImage({item,index}){
    
    let {dataList}=this.state;
    let targetPost=dataList[index];
    let action="0";

    if(targetPost.isLiked==0)
    {
      targetPost.isLiked=1
      targetPost.likecount=targetPost.likecount+1
      action="1";
    }else{
    if(targetPost.isLiked==1)
    {
      targetPost.isLiked=0
      targetPost.likecount=targetPost.likecount-1
      action="0";
    }
    }
    this.setState({dataList})

    AsyncStorage.getItem('userToken',(err,resultToken)=>{
       
      const response = fetch(
        "https://qa.openpixs.com/opImageLikeUnlike", {
            method: 'POST',
            headers: {
            Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authentication' : resultToken,
          },
          body: JSON.stringify({
            imgGuid:targetPost.imgGuid,
	          action:action,
          }),
          }) 
          .then((response) => { return  response.json() } ) 
          .catch((error) => console.warn("fetch error:", error))
          .then((response) => {
            console.log(JSON.stringify(response));
          });
        });
  }

  _onDownloadImage(imageId){
    AsyncStorage.getItem('userToken',(err,result)=>{
      const response = fetch(
        "https://qa.openpixs.com/opSendDownloadEmail?imgGuid="+imageId, {
            method: 'GET',
            headers: {
            Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authentication' : result,
                
          },
          }) 
          .then((response) => { return  response.json() } ) 
          .catch((error) => console.warn("fetch error:", error))
          .then((response) => {
            console.log(JSON.stringify(response));
            Toast.show(JSON.stringify(response));
          })
        });
  }

  renderForm=(originalDate)=>{
    var toDayDate=new Date();
    var myDate=new Date(originalDate);
    var millisec=Math.abs(toDayDate.getTime()-myDate.getTime());
    
    var finalTime=null
    var seconds = (millisec / 1000).toFixed(1);

    var minutes = (millisec / (1000 * 60)).toFixed(1);

    var hours = (millisec / (1000 * 60 * 60)).toFixed(1);

    var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);

    if (seconds < 60) {
        finalTime= seconds + " s";
    } else if (minutes < 60) {
      finalTime= minutes + " min";
    } else if (hours < 24) {
      finalTime= hours + " Hr";
    } else {
      finalTime= Math.round(days) + " D"
    }
    
      // this.setState({
      //   statusTime:finalTime,
      // })
      return(
        <Text>{finalTime}</Text>
      )
  }

  _renderTagCount=(orgStr)=>{
    var str=orgStr
        var tag_array=str.split('#');

        for(var i=0;i<tag_array.length;i++){
          tag_array[i]=tag_array[i].replace(/^\s*/,"").replace(/\s*$/,"");

        }
        return(
          <View style={{flexDirection:'column'}}>
          <Text style={{alignSelf:'center',fontSize:16}}>{tag_array.length-1}</Text>
          {tag_array.length==1?
          <TouchableOpacity onPress={()=>{this.setTagsVisible(item.tags)}}>
              <Image source={{uri:'https://i.imgur.com/heXcngg.png'}}
                  style={{width:25,height:25,marginLeft:4}}></Image>
          </TouchableOpacity>
          :
          <Image source={{uri:'https://i.imgur.com/heXcngg.png'}}
                  style={{width:25,height:25,marginLeft:4}}></Image>
          }
          </View>
        ); 
  }

  _renderTruncatedFooter = (handlePress) => {
    return (
      <Text style={{color: 'grey', marginTop: 5}} onPress={handlePress}>
        Read more
      </Text>
    );
  }

  _renderRevealedFooter = (handlePress) => {
    return (
      <Text style={{color: 'grey', marginTop: 5}} onPress={handlePress}>
        Show less
      </Text>
    );
  }

  _followUnFollow({item,index}){
    let {dataList}=this.state;
    let targetPost=dataList[index];
    targetPost.followStatus=1
    this.setState({dataList})

    AsyncStorage.getItem('userToken',(err,result)=>{

    const response = fetch(
      "https://qa.openpixs.com/opFollowUnfollowUser", {
          method: 'POST',
          headers: {
          Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authentication' : result,
        },
        body: JSON.stringify({
          userid:targetPost.guid,
          action:1
        }),
        }) 
        .then((response) => { return  response.json() } ) 
        .catch((error) => console.warn("fetch error:", error))
        .then((response) => {
          console.log(JSON.stringify(response));
          if(response.status==400||response.status==500){
            Alert.alert(JSON.stringify(response.errormsg));
          }
          else{
            Toast.show("User Followed")
          }
        })
      })
  }

   render()
   {
    BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);

    var isLikedImg=true
     if(this.state.isFirst){
       this.fetchData()
     }
      return(
        <View>
         <View>
            <FlatList
            onEndReached={()=>this._onFetchNext()}
            onScroll={({nativeEvent})=>{
              if(this.isCloseToBottom(nativeEvent)){
                this._onRefresh()
              }
            }}
            onEndReachedThreshold={0.7}
            extraData={this.state}
            data={this.state.dataList}
            renderItem={({item,index})=>
            <View style={{flex:1,marginBottom:10}}>
                  <View style={{flexDirection:'row',justifyContent:'space-between',height:25,margin:8}}>
                  
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Eleventh',{userID:item.guid,isRefreshing:true})}>
                      <View style={{flexDirection:'row'}}>
                        {item.profile_image_url==null?
                            <Icon name="account-circle" size={45} color="#6423a2"></Icon>
                            :
                          <Image style={{marginRight :4,height:40,width:40, borderRadius:20}} source={{uri:
                              item.profile_image_url}} ></Image>
                        }  
                        <View style={{flexDirection:'column',justifyContent:'center',height:40,}}> 
                          <Text style={{fontSize:16}}>{item.name}</Text>
                          <Text style={{fontSize:12}}>{item.location}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  <View>
                    {item.followStatus==0?
                    <Button onPress={()=>this._followUnFollow({item,index})} style={{backgroundColor:'#6423a2',paddingLeft:12,paddingRight:12,height:34,alignSelf:'center'}}>
                        <Text style={{color:'white'}}>Follow</Text>
                    </Button>
                    :
                      null
                    }
                  </View>      
                  <View style={{flexDirection:'column',marginTop:4}}>
                    <View style={{flexDirection:'row'}}>
                      {this._renderTagCount(item.tags)}
                    </View>
                    <View style={{marginLeft:8}}>
                    {this.renderForm(item.upload_date)}
                    </View>   
                  </View>
                </View>
                
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Tenth',
                        {imgSrc:item.imgGuid,isRefreshing:true})}> 
              <ImageBackground style={{height:300,marginTop:16,flexDirection:'column-reverse'}} source=
                {{uri:"https://d1n5ldzwzxqojp.cloudfront.net/"+item.guid+"/"+item.path}}>
              
              </ImageBackground>
              </TouchableOpacity>
              <View style={{height:25,flexDirection:'row',paddingLeft:40,justifyContent:'space-between',marginTop:8}}>
                <View style={{height:25,flexDirection:'row',}}>
                  <Image style={{width:25,height:25,}} source={{uri:'https://i.imgur.com/5TTsKOj.png'}}></Image>
                    <View style={{alignContent:'center',height:25,alignItems:'center',alignSelf:'center'}}>
                      <Text style={{marginRight:40,color:'grey',fontWeight:'bold',marginLeft:8}}>{item.imageViewCount+" Views"}</Text>
                    </View>
                </View>
                
                <View style={{height:25,flexDirection:'row',paddingLeft:40}}>
                
                    <Icon
                      name="favorite"
                      size={25}
                      color={item.isLiked==1? 'red':'grey'}
                      onPress={()=>this._onLikeImage({item,index})}>
                    </Icon>
                  {/* {item.isLiked==0?
                  <TouchableOpacity onPress={()=>this._onLikeImage(item.imgGuid,index,"1")}>
                    <Image  style={{width:25,height:25}} source={{uri:"https://i.imgur.com/3Ak4Qch.png"}}></Image>
                  </TouchableOpacity>
                    :
                  <TouchableOpacity onPress={()=>this._onLikeImage(item.imgGuid,"0")}>
                    <Image style={{width:25,height:25}} source={{uri:"https://i.imgur.com/4ermCMf.png"}}></Image>
                  </TouchableOpacity>
                  }  */}
                  
                  <View style={{alignContent:'center',height:25,alignItems:'center'}}>
                    <Text style={{marginRight:40,color:'grey',fontWeight:'bold',marginLeft:4}}>{item.likecount+" Likes"}</Text>
                  </View>
                </View>
              </View>

              <View style={{marginLeft:8,marginRight:8,marginTop:8,marginBottom:4,flexDirection:'row'}}>
                <View style={{width:'90%'}}>
                <ReadMore
                  numberOfLines={2}
                  renderTruncatedFooter={this._renderTruncatedFooter}
                  renderRevealedFooter={this._renderRevealedFooter}
                  >
                  <Text>
                    {item.description}
                  </Text>
                </ReadMore>
                </View>
                <TouchableOpacity onPress={()=>this._onDownloadImage(item.imgGuid)}>
                <Image style={{width:35,height:35}} source={{uri:'https://i.imgur.com/OsHsdbu.png'}}></Image>
                </TouchableOpacity>
              </View>
              <View style={{elevation:1,height:1,backgroundColor:'white'}}></View>
          </View>
          
          }
          keyExtractor={(item,index)=>'list-item-${index}'}>
          </FlatList>
         </View>
         </View>
      );
  }
}

class TagsPage extends Component{
  constructor(props) {
 
    super(props);
    this.state={
      isLoading:true,
      tags:''
    }
  
  }
  render(){
    const {state}=this.props.navigation;
        if(state.params.isRefreshing){
          this.setState({
            tags:state.params.tags
          })
          state.params.isRefreshing=false;
        }
        var str=this.state.tags
        var tag_array=str.split('#');
        for(var i=0;i<tag_array.length;i++){
          tag_array[i]=tag_array[i].replace(/^\s*/,"").replace(/\s*$/,"");

        } 
    return(
      <View style={{height:300,alignSelf:'center',flex:1,justifyContent:'flex-start'}}>
        <View style={{alignSelf:'center',width:200,padding:8}}>
          <Text style={{fontSize:16,fontWeight:'bold',alignSelf:'center'}}>#TAGS</Text>
          <Text style={{fontSize:16,fontWeight:'bold',alignSelf:'center'}}>
              {this.state.tags}</Text>
          <FlatList
            style={{width:200}}
            data={tag_array}
            renderItem={({item})=>
            <View>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Seventh',{search:item,isRefreshing:true})}}>
              <Text style={{fontSize:18,margin:4,width:100}}>{"#"+item}</Text>
              </TouchableOpacity>
            </View>
          }
          numColumns={2}
          keyExtractor={(item,index)=>'list-item-${index}'}>
          </FlatList>
          <Button onPress={()=>{this.props.navigation.navigate('Sixth')}} 
              style={{width:200,backgroundColor:'white',justifyContent:'center',marginTop:8}}>
            <Text style={{fontSize:16,fontWeight:'bold',alignSelf:'center'}}>Close</Text>
          </Button>
        </View>
      </View>
    );
  }
}


//Profile
// class SecondActivity extends Component {

//   constructor(props) {
 
//     super(props);
//     this.state={
//       isLoading:true,
//       dataList:[],
//       imageList:[],
//       userID:'',
//       isRefreshing:false,
//       uploadType:''
//     }
  
//   }

//   _fetchData(){

//     this.setState({
//       isRefreshing:true,
//     })

//     AsyncStorage.getItem('userGuid',(err,resultID)=>{
//       AsyncStorage.getItem('userToken',(err,resultToken)=>{
//         const response = fetch(
//           "https://qa.openpixs.com/opUserDashboardStats?id="+resultID, {
//               method: 'GET',
//               headers: {
//               Accept: 'application/json',
//                   'Content-Type': 'application/json',
//                   'Authentication' : resultToken,
//             },
//             }) 
//             .then((response) => { return  response.json() } ) 
//             .catch((error) => console.warn("fetch error:", error))
//             .then((response) => {
//               console.log(JSON.stringify(response));
//               this.setState({
//                 dataList:response,
//                 isLoading:false,
//                 userID:resultID,
//                 isRefreshing:false
//               });
//             })
//       });
//     });

        
//     AsyncStorage.getItem('userGuid',(err,resultID)=>{
//       AsyncStorage.getItem('userToken',(err,resultToken)=>{
//         const responseImages = fetch(
         
//           "https://qa.openpixs.com/opGetPortfolioImages?id="+resultID+"&limit=50&offset=0", {
//               method: 'GET',
//               headers: {
//               Accept: 'application/json',
//                   'Content-Type': 'application/json',
//                   'Authentication' : resultToken,
                  
//             },
//             }) 
//             .then((response) => { return  response.json() } ) 
//             .catch((error) => console.warn("fetch error:", error))
//             .then((response) => {
//               console.log(JSON.stringify(response));
//               this.setState({
//                 imageList:response,
//                 isLoading:false,
               
//               });
//             })
//           });
//         });
//   }

//   _onRefresh=()=>{
//     this._fetchData();
//   }

//   componentDidMount(){
//       this._fetchData();
//      // this.interval=setInterval(()=>this._fetchData,this.setState({time:Date.now()}),10000)  
//   }

//   uploadProfileImage(picType){
//     const options = {
//       quality: 1.0,
//       maxWidth: 500,
//       maxHeight: 500,
//       storageOptions: {
//         skipBackup: true,
//       },
// };
//   if(picType=='profile')
//     this.setState({
//       uploadType:'profile'
//     })

//     if(picType=='cover')
//     this.setState({
//         uploadType:'cover'
//     })
//     ImagePicker.showImagePicker(options, (response) => {
//       console.log('Response = ', response);

//       if (response.didCancel) {
//         console.log('User cancelled photo picker');
//       } else if (response.error) {
//         console.log('ImagePicker Error: ', response.error);
//       } else if (response.customButton) {
//         console.log('User tapped custom button: ', response.customButton);
//       } else {
//        // let source = { uri: response.uri };

//         // You can also display the image using data:
//         // let source = { uri: 'data:image/jpeg;base64,' + response.data };
//         // let source = response.data
//         // Toast.show(source.toString)
         
//         const RNFS=require('react-native-fs');
//         RNFS.readFile(response.uri,"base64").then(data=>{
//           Toast.show(data)
//           this.uploadImageS3(data)
//         })
        
//       }
//     });
// }

// uploadImageS3(base64Image){

//   AsyncStorage.getItem('userToken',(err,resultToken)=>{
       
//     const response = fetch(
//       "https://qa.openpixs.com/opS3UploadImage", {
//           method: 'POST',
//           headers: {
//           Accept: 'application/json',
//               'Content-Type': 'application/json',
//               'Authentication' : resultToken,
//         },
//         body: JSON.stringify({
//           uploadType:"public",
//           base64String:base64Image
//         }),
//         }) 
//         .then((response) => { return  response.json() } ) 
//         .catch((error) => console.warn("fetch error:", error))
//         .then((response) => {
//           console.log(JSON.stringify(response));
//           Toast.show(JSON.stringify(response));
//           this.updateProfile(response.path)
//         });
//       });
// }

// updateProfile(path){
//   if(this.state.uploadType=='profile'){

//   AsyncStorage.getItem('userToken',(err,resultToken)=>{
//     AsyncStorage.getItem('userGuid',(err,userGuid)=>{      
//     const response = fetch(
//       "https://qa.openpixs.com/opUpdateUser", {
//           method: 'POST',
//           headers: {
//           Accept: 'application/json',
//               'Content-Type': 'application/json',
//               'Authentication' : resultToken,
//         },
//         body: JSON.stringify({
//           profile_image_url:'https://d1n5ldzwzxqojp.cloudfront.net/'+userGuid+"/"+path
//         }),
//         }) 
//         .then((response) => { return  response.json() } ) 
//         .catch((error) => console.warn("fetch error:", error))
//         .then((response) => {
//           console.log(JSON.stringify(response));
//           Toast.show(JSON.stringify(response),Toast.SHORT);
//           Toast.show('https://d1n5ldzwzxqojp.cloudfront.net/'+userGuid+"/"+path,Toast.LONG);
//         });
//       });
//     });
//     }

//     if(this.state.uploadType=='cover'){
//       AsyncStorage.getItem('userToken',(err,resultToken)=>{
           
//         const response = fetch(
//           "https://qa.openpixs.com/opUpdateUser", {
//               method: 'POST',
//               headers: {
//               Accept: 'application/json',
//                   'Content-Type': 'application/json',
//                   'Authentication' : resultToken,
//             },
//             body: JSON.stringify({
//               cover_image_url_1:path
//             }),
//             }) 
//             .then((response) => { return  response.json() } ) 
//             .catch((error) => console.warn("fetch error:", error))
//             .then((response) => {
//               console.log(JSON.stringify(response));
//               Toast.show(JSON.stringify(response),Toast.SHORT);
//               Toast.show(this.state.uploadType);

//             });
//           });
//         }

// }
   
//      render()
//      {
//        if(this.state.isLoading){
        
//         return(
//           <View style={{flex:1,padding:8}}>
//            <ActivityIndicator/>
//           </View>
//         )
//       }
//       else{
        
//         return(
//            <View style={{flex:1}}>
//               <ScrollView style={{flex:1}}
//               refreshControl={
//                 <RefreshControl
//                   refreshing={this.state.isRefreshing}
//                   onRefresh={this._onRefresh}
//                 ></RefreshControl>} 
//               >
//                 <TouchableOpacity onPress={()=>{this.uploadProfileImage('cover')}}>
//                 <Image style={{height:250,backgroundColor:'#9370DB'}}
//                   source={{uri:this.state.dataList.coverImage1}}>
//                 </Image>
//                 </TouchableOpacity>
//                 <View style={{height:85,flexDirection:'row',backgroundColor:'#fff',padding:16,
//                   justifyContent:'space-between'}}>
                  
//                   <View style={{flexDirection:'row',alignContent:'center',alignItems:'center',
//                     justifyContent:'center'}}>
//                     <TouchableOpacity onPress={()=>{this.uploadProfileImage('profile')}}>
//                     {this.state.dataList.profileImageUrl==null?
//                       <Icon style={{marginRight :4}} name='account-circle' color='#6423a2' size={50}></Icon>
//                       :
//                       <Image style={{width:40,height:40,borderRadius:20,}} source={{uri:this.state.dataList.profileImageUrl}}></Image>
//                     }
//                     </TouchableOpacity>
//                     <Text style={{marginLeft:8,alignSelf:'center',fontWeight:'bold'}}>{this.state.dataList.name}</Text>
//                   </View>  
//                 </View>

//                 <View style={{height:85,flexDirection:'row',backgroundColor:'#fff',padding:16,justifyContent:'space-between'}}>
//                     <TouchableOpacity onPress={()=>this.props.navigation.navigate('Twelveth',
//                     {userId:this.state.dataList.userid,isFollow:1})}>
//                     <View style={{flexDirection:'column'}}>
//                       <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalfollowers}</Text>
//                       <Text style={{fontSize:10,alignSelf:'center'}}>FOLLOWERS</Text>
//                     </View>
//                     </TouchableOpacity>
//                     <TouchableOpacity onPress={()=>this.props.navigation.navigate('Twelveth',
//                     {userId:this.state.dataList.userid,isFollow:0})}> 
//                     <View style={{flexDirection:'column'}}>
//                       <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalfollowing}</Text>
//                       <Text style={{fontSize:10,alignSelf:'center'}}>FOLLOWING</Text>
//                     </View>
//                     </TouchableOpacity> 
//                     <View style={{flexDirection:'column'}}>
//                       <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalimages}</Text>
//                       <Text style={{fontSize:10,alignSelf:'center'}}>IMAGE UPLOADED</Text>
//                     </View>
//                     <View style={{flexDirection:'column'}}>
//                       <Text style={{fontSize:20,alignSelf:'center'}}>4</Text>
//                       <Text style={{fontSize:10,alignSelf:'center'}}>USER LEVEL</Text>
//                     </View>
//                   </View>
//                   <View >
//                   <FlatList
//                       data={this.state.imageList}
//                       renderItem={({item})=>
//                         <View style={{margin:8}}>
//                         <TouchableOpacity onPress={()=>this.props.navigation.navigate('Tenth',
//                         {imgSrc:item.id,isRefreshing:true})}>
//                           <Image style={{width:100,height:100,backgroundColor:'grey'}} source={{uri:
//                             "https://d1n5ldzwzxqojp.cloudfront.net/"+this.state.userID+"/"+item.path}}></Image>
//                         </TouchableOpacity>
//                         </View>
//                       }
//                       numColumns={3}
//                       keyExtractor={(item,index)=>index}>
//                       </FlatList>
//                   </View>
                
//               </ScrollView>
              
//            </View>
//         );
//      }
//     }
//   }

// class UserDashboardActivity extends Component {

//     constructor(props) {
   
//       super(props);
//       this.state={
//         isLoading:true,
//         dataList:[],
//         imageList:[],
//         userID:'',
//         isRefreshing:false,
//         isSecond:false,
//       }
    
//     }
//     handleBackButton=()=>{
//       this.props.navigation.navigate('Sixth');
//       return true;
//     }

//     _followUnFollow(userId,action){
//       AsyncStorage.getItem('userToken',(err,result)=>{

//       const response = fetch(
//         "https://qa.openpixs.com/opFollowUnfollowUser", {
//             method: 'POST',
//             headers: {
//             Accept: 'application/json',
//                 'Content-Type': 'application/json',
//                 'Authentication' : result,
//           },
//           body: JSON.stringify({
//             userid:userId,
//             action:action
//           }),
//           }) 
//           .then((response) => { return  response.json() } ) 
//           .catch((error) => console.warn("fetch error:", error))
//           .then((response) => {
//             console.log(JSON.stringify(response));
//             if(response.status==400||response.status==500){
//               Alert.alert(JSON.stringify(response.errormsg));
//             }
//             else{
//               this._fetchData();
//             }
//           })
//         })
//     }
  
//     _fetchData(){
//       const {state}=this.props.navigation; 

//         this.setState({
//           userID:state.params.userID,
//         })
//         AsyncStorage.getItem('userToken',(err,resultToken)=>{
//           const response = fetch(
//             "https://qa.openpixs.com/opUserDashboardStats?id="+this.state.userID, {
//                 method: 'GET',
//                 headers: {
//                 Accept: 'application/json',
//                     'Content-Type': 'application/json',
//                     'Authentication' : resultToken,
//               },
//               }) 
//               .then((response) => { return  response.json() } ) 
//               .catch((error) => console.warn("fetch error:", error))
//               .then((response) => {
//                 console.log(JSON.stringify(response));
//                 this.setState({
//                   dataList:response,
//                   isLoading:false,
//                   isRefreshing:false,
//                   isSecond:true
//                 });
//               })
//         });
  
//         AsyncStorage.getItem('userToken',(err,resultToken)=>{
//           const responseImages = fetch(
           
//             "https://qa.openpixs.com/opGetPortfolioImages?id="+this.state.userID+"&limit=50&offset=0", {
//                 method: 'GET',
//                 headers: {
//                 Accept: 'application/json',
//                     'Content-Type': 'application/json',
//                     'Authentication' : resultToken,
                    
//               },
//               }) 
//               .then((response) => { return  response.json() } ) 
//               .catch((error) => console.warn("fetch error:", error))
//               .then((response) => {
//                 console.log(JSON.stringify(response));
//                 this.setState({
//                   imageList:response,
//                   isLoading:false,
                 
//                 });
//               })
//             });
//     }
  
//     componentDidMount(){
//         BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
//         this._fetchData();
//        // this.interval=setInterval(()=>this._fetchData,this.setState({time:Date.now()}),10000)  
//     }
  
    
     
//        render()
//        {
//         if(this.state.isLoading){
//           return(
//             <View style={{flex:1,padding:8}}>
//              <ActivityIndicator/>
//             </View>
//           )
//         }
         
//         const {state}=this.props.navigation;
//         if(state.params.isRefreshing){
//           if(this.state.isSecond){
//           this._fetchData();
//           state.params.isRefreshing=false;
//           }
//         }

//         if(this.state.isLoading){
          
//           return(
//             <View style={{flex:1,padding:8}}>
//              <ActivityIndicator/>
//             </View>
//           )
//         }
//         else{
          
//           return(
            
//              <View style={{flex:1}}>
//                 <ScrollView style={{flex:1}}>
//                   <Image style={{height:230,backgroundColor:'#9370DB'}}
//                     source={{url:this.state.dataList.cover_image_url_1}}>
//                   </Image>
//                   <View style={{height:85,flexDirection:'row',backgroundColor:'#fff',padding:16,
//                     justifyContent:'space-between'}}>
                    
//                     <View style={{flexDirection:'row',alignContent:'center',alignItems:'center',
//                       justifyContent:'center'}}>
//                       {this.state.dataList.profileImageUrl==null?
//                         <Icon style={{marginRight :4}} name='account-circle' color='#6423a2' size={50}></Icon>
//                         :
//                         <Image style={{width:40,height:40,borderRadius:20,}} source={{uri:this.state.dataList.profileImageUrl}}></Image>
//                       }
//                       <Text style={{marginLeft:8,alignSelf:'center',fontWeight:'bold'}}>{this.state.dataList.name}</Text>
//                     </View>
//                     {this.state.dataList.isFollow==0?  
//                     <Button onPress={()=>this._followUnFollow(this.state.dataList.userid,"1")} style={{backgroundColor:'#6423a2',paddingLeft:12,paddingRight:12,height:34,alignSelf:'center'}}>
//                       <Text style={{color:'white'}}>Follow</Text>
//                     </Button>  
//                       :
//                     <Button onPress={()=>this._followUnFollow(this.state.dataList.userid,"0")} style={{backgroundColor:'#6423a2',paddingLeft:12,paddingRight:12,height:34,alignSelf:'center'}}>  
//                       <Text style={{color:'white'}}>UnFollow</Text>
//                     </Button>  
//                     }    
//                   </View>
  
//                   <View style={{height:85,flexDirection:'row',backgroundColor:'#fff',padding:16,justifyContent:'space-between'}}>
//                     <TouchableOpacity onPress={()=>this.props.navigation.navigate('Twelveth',
//                     {userId:this.state.dataList.userid,isFollow:1})}>
//                     <View style={{flexDirection:'column'}}>
//                       <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalfollowers}</Text>
//                       <Text style={{fontSize:10,alignSelf:'center'}}>FOLLOWERS</Text>
//                     </View>
//                     </TouchableOpacity>
//                     <TouchableOpacity onPress={()=>this.props.navigation.navigate('Twelveth',
//                     {userId:this.state.dataList.userid,isFollow:0})}> 
//                     <View style={{flexDirection:'column'}}>
//                       <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalfollowing}</Text>
//                       <Text style={{fontSize:10,alignSelf:'center'}}>FOLLOWING</Text>
//                     </View>
//                     </TouchableOpacity> 
//                     <View style={{flexDirection:'column'}}>
//                       <Text style={{fontSize:20,alignSelf:'center'}}>{this.state.dataList.totalimages}</Text>
//                       <Text style={{fontSize:10,alignSelf:'center'}}>IMAGE UPLOADED</Text>
//                     </View>
//                     <View style={{flexDirection:'column'}}>
//                       <Text style={{fontSize:20,alignSelf:'center'}}>4</Text>
//                       <Text style={{fontSize:10,alignSelf:'center'}}>USER LEVEL</Text>
//                     </View>
//                   </View>
//                   <View >
//                   <FlatList
//                       data={this.state.imageList}
//                       renderItem={({item})=>
//                         <View style={{margin:8}}>
//                         <TouchableOpacity onPress={()=>this.props.navigation.navigate('Tenth',
//                         {imgSrc:item.id,isRefreshing:true})}>
//                           <Image style={{width:100,height:100,backgroundColor:'grey'}} source={{uri:
//                             "https://d1n5ldzwzxqojp.cloudfront.net/"+this.state.userID+"/"+item.path}}></Image>
//                         </TouchableOpacity>
//                         </View>
//                       }
//                       numColumns={3}
//                       keyExtractor={(item,index)=>index}>
//                       </FlatList>
//                   </View>
                  
//                 </ScrollView>
                
//              </View>
//           );
//        }
//       }
//     }
  //Setting
  // class ThirdActivity extends Component {

  //   constructor(props) {
 
  //     super(props);
    
  //     this.state={
  //       isLoading:true,
  //       dataList:[],
  //       userName:'',
  //       description:'',
  //       showEditables:false
  //     }

  //     YellowBox.ignoreWarnings([
  //      'Warning: componentWillMount is deprecated',
  //      'Warning: componentWillReceiveProps is deprecated',
  //    ]);
    
  //   }


  //   componentDidMount(){
    
  //     AsyncStorage.getItem('userGuid',(err,resultID)=>{
  //       AsyncStorage.getItem('userToken',(err,resultToken)=>{  
  //         const response = fetch(
  //           // ID Modify
  //           "https://qa.openpixs.com/opUserDashboardStats?id="+resultID, {
  //               method: 'GET',
  //               headers: {
  //               Accept: 'application/json',
  //                   'Content-Type': 'application/json',
  //                   'Authentication' : resultToken,
  //                   // Modify
  //             },
  //             }) 
  //             .then((response) => { return  response.json() } ) 
  //             .catch((error) => console.warn("fetch error:", error))
  //             .then((response) => {
  //               console.log(JSON.stringify(response));
  //               this.setState({
  //                 dataList:response,
  //                 isLoading:false,
  //               });
  //             })
  //           });
  //         });
  //   }

  //   userUpdate(){
  //     this.setState({
  //       showEditables:false
  //     })
  //     AsyncStorage.getItem('userToken',(err,resultToken)=>{
       
  //       const response = fetch(
  //         "https://qa.openpixs.com/opUpdateUser", {
  //             method: 'POST',
  //             headers: {
  //             Accept: 'application/json',
  //                 'Content-Type': 'application/json',
  //                 'Authentication' : resultToken,
  //           },
  //           body: JSON.stringify({
  //             deactivateAccount : 0,
  //             name:this.state.userName,
  //             description:this.state.description,
  //           }),
  //           }) 
  //           .then((response) => { return  response.json() } ) 
  //           .catch((error) => console.warn("fetch error:", error))
  //           .then((response) => {
  //             console.log(JSON.stringify(response));
  //             Toast.show(JSON.stringify(response),Toast.SHORT);
  //           });
  //         });
  //   }
 
  //      render()
  //      {
  //       if(this.state.isLoading){
  //         return(
  //           <View style={{flex:1,padding:8}}>
  //            <ActivityIndicator/>
  //           </View>
  //         )
  //       }
  //       else{
  //         return(
            
  //            <View >
  //              <View style={{flexDirection:'row',justifyContent:'space-between'}}>
  //                 <View style={{flexDirection:'row',height:90,padding:16}}>
  //                   {this.state.dataList.profileImageUrl==null?
  //                     <Icon style={{marginRight :4}} name='account-circle' color='#6423a2' size={50}></Icon>
  //                     :
  //                     <Image style={{width:50,height:50,borderRadius:25,}} source={{uri:this.state.dataList.profileImageUrl}}></Image>
  //                   } 
  //                   <Text style={{marginLeft:16,fontSize:16,fontWeight:'bold',alignSelf:'center'}}>{this.state.dataList.name}</Text>
  //                 </View>
  //                 {this.state.showEditables?
  //                 <TouchableOpacity style={{alignSelf:'center'}} onPress={()=>this.userUpdate()}> 
  //                 <View style={{backgroundColor:'silver',height:40,width:40,borderRadius:150/2,alignSelf:'center',marginRight:8,padding:8}}>
  //                   <Icon style={{alignSelf:'center'}} name='save' color='#6423a2' size={25}></Icon>
  //                 </View>
  //                 </TouchableOpacity>
  //                 :
  //                 <TouchableOpacity style={{alignSelf:'center'}} onPress={()=>this.setState({showEditables:true})}> 
  //                 <View style={{backgroundColor:'silver',height:40,width:40,borderRadius:150/2,alignSelf:'center',marginRight:8,padding:8}}>
  //                   <Icon style={{alignSelf:'center'}} name='edit' color='#6423a2' size={25}></Icon>
  //                 </View>
  //                 </TouchableOpacity> 
  //                 }
  //             </View>
  //             {this.state.showEditables?
  //             <View >
  //               <TextInput
                      
  //                     style={{width:'100%',alignSelf:'center',height:40,fontSize:16,borderColor:'#EAEAEA',borderWidth:1,marginLeft:16}} 
  //                     placeholder="Enter New Name"
  //                     placeholderTextColor = 'grey'
  //                     selectionColor="#6423a2"
  //                     keyboardType="default"
  //                     onChangeText={(text)=>this.setState({userName:text})}
  //                     />
  //               <TextInput 
  //                     style={{width:'100%',height:100,fontSize:16,
  //                     borderColor:'#EAEAEA',borderWidth:1,margin:8,padding:8}} 
  //                     placeholder="Enter New Description"
  //                     placeholderTextColor = 'grey'
  //                     selectionColor="#6423a2"
  //                     keyboardType="default"
  //                     onChangeText={(text)=>this.setState({description:text})}
  //                     />
  //               </View> 
  //               :
  //               null
  //             }     
  //             <View style={{justifyContent:'center',alignItems:'flex-start',flexDirection:'column',marginLeft:16}}>
  //               <TouchableOpacity onPress={()=>this.userUpdate()}>
  //               <View style={{flexDirection:'row'}}>
  //                   <Icon name='account-box' color='#6423a2' size={30} ></Icon>
  //                   <Text style={{marginLeft:8,alignSelf:'center',fontWeight:'bold',width:150}}>Deactivate Account</Text>
  //               </View>
  //               </TouchableOpacity>
  //               <TouchableOpacity onPress={()=>this.props.navigation.navigate('Thirteen',{id:'privacy',isRefreshing:true})}>
  //               <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:16}}>
  //                   <Icon  name='lock' color='#6423a2' size={30} ></Icon>
  //                   <Text style={{marginLeft:8,alignSelf:'center',fontWeight:'bold',width:150}}>Privacy</Text>
  //               </View>
  //               </TouchableOpacity>
  //               <TouchableOpacity onPress={()=>this.props.navigation.navigate('Thirteen',{id:'faq',isRefreshing:true})}>
  //               <View style={{flexDirection:'row',marginTop:16}}>
  //                   <Icon name='help' color='#6423a2' size={30}></Icon>
  //                   <Text style={{marginLeft:8,alignSelf:'center',fontWeight:'bold',width:150}}>FAQ's</Text>
  //               </View>
  //               </TouchableOpacity>
  //             </View>  
  //            </View>
  //         );
  //         }
  //       }
  //   }
 
    const FirstActivity_StackNavigator = createStackNavigator({
      First: { 
        screen: MainActivity, 
        navigationOptions: ({ navigation }) => ({
          
          headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    // class Login extends React.Component {

    //   constructor(props){
    //     super(props);
    //     this.state={
    //       isLoading:false,
    //       dataSource:null,
    //       userEmail:'',
    //       userPassword:''
          
    //     }
        
    //   }
    //   handleBackButton=()=>{
    //     // BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
    //     this.props.navigation.navigate('Sixth');
    //     return true;
      
    //   }
      

    //   componentDidMount(){
    //     this.setState({})
    //     AsyncStorage.getItem('userEmail',(err,userEmail)=>{
    //         AsyncStorage.getItem('userPassword',(err,userPassword)=>{
    //           if(userEmail!=null){
    //             Toast.show("Logging In....")
    //             this.signIn(userEmail,userPassword);
    //           }
    //           else{
    //             SplashScreen.hide();
    //           }
    //         })
                  
    //     })
        
    //   }
    //   updateUI(){
    //     return(
            
    //       <View style={{flex:1,padding:8,alignItems:'center',alignSelf:'center'}}>
    //        <ActivityIndicator size="large"/>
    //       </View>
    //     )
    //     BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
    //   }
  
    //   signIn(userEmail,userPassword){
    //     this.updateUI();
    //     this.setState({
    //       isLoading:true,
    //     })
    //     const response = fetch(
    //       "https://qa.openpixs.com/opAuthenticateUser", {
    //           method: 'POST',
    //           headers: {
    //           Accept: 'application/json',
    //               'Content-Type': 'application/json',
    //         },
    //         body: JSON.stringify({
    //           email: userEmail,
    //           password: userPassword,
    //         }),
    //         }) 
    //         .then((response) => { return  response.json() } ) 
    //         .catch((error) => console.warn("fetch error:", error))
    //         .then((response) => {
    //           console.log(JSON.stringify(response));
    //           if(response.status==400){
    //             Alert.alert(JSON.stringify(response.errormsg));
    //           }
    //           else{
    //             this.props.navigation.navigate('Sixth');
    //             AsyncStorage.setItem('userName',response.name);
    //             AsyncStorage.setItem('userGuid',response.guid);
    //             AsyncStorage.setItem('userToken',response.token);
    //             AsyncStorage.setItem('userEmail',userEmail);
    //             AsyncStorage.setItem('userPassword',userPassword);
                
    //           }
    //         })
    //         if(this.password!=null){
    //         this.password.clear();
    //         this.userEmail.clear();
    //       }
    //         this.setState({
    //           userEmail:'',
    //           userPassword:'',
    //           isLoading:false,
    //         })
            
    //   }

    //   render() {
    //     const {navigate}=this.props.navigation;
    //     if(this.state.isLoading){
    //       return(
    //         <View style={{flex:1,padding:8,alignItems:'center',alignSelf:'center'}}>
    //          <ActivityIndicator/>
    //         </View>
    //       )
    //     }
    //     return(
    //       <View style={styles.containerLogin}>
    //         <View style={styles.containerForm}>
              
    //           <Image source={{uri:'https://i.imgur.com/OgZY6Cw.png'}} style={{width:250,height:150}}></Image>
    //           <View style={{flexDirection:'row',borderBottomColor:'#6423a2',
    //           borderBottomWidth:2,margin:16}}>
    //             <Icon style={{alignSelf:'center'}} 
    //             name='email' color='#6423a2' size={30}></Icon>
    //             <TextInput style={styles.inputBox} 
    //                 placeholder="Email"
    //                 placeholderTextColor = 'grey'
    //                 selectionColor="#6423a2"
    //                 keyboardType="email-address"
    //                 ref={(input)=>this.userEmail=input}
    //                 onChangeText={(text)=>this.setState({userEmail:text})}
                    
    //                 />
    //           </View>  
    //           <View style={{flexDirection:'row',borderBottomColor:'#6423a2',borderBottomWidth:2,margin:16}}>
    //             <Icon style={{alignSelf:'center'}} 
    //             name='lock' color='#6423a2' size={30}></Icon>  
    //             <TextInput style={styles.inputBox}  
    //                 placeholder="Password"
    //                 secureTextEntry={true}
    //                 placeholderTextColor = "grey"
    //                 ref={(input) => this.password = input}
    //                 onChangeText={(text)=>this.setState({userPassword:text})}
    //                 />
    //           </View> 
    //           {/* //onPress={()=>navigate('Sixth')}        */}
    //           <TouchableOpacity style={styles.button} onPress={()=>this.signIn(this.state.userEmail,
    //             this.state.userPassword)}>
    //             <Text style={styles.buttonText}>Login</Text>
    //           </TouchableOpacity>     
    //         </View>
    //         <View style={styles.signupTextCont}>
    //           <Text style={styles.signupText}>Don't have an account yet?</Text>
    //           <TouchableOpacity onPress={()=>navigate('Fifth')} style={styles.buttonSignUp}>
    //           <Text style={styles.signupButton}> Signup</Text></TouchableOpacity>
    //         </View>
    //       </View>	
    //       )

          
    //   }

      
      
    // }

    // class Signup extends React.Component {
    //   constructor(props){
    //     super(props);
    //     this.state={
    //       isLoading:false,
    //       dataSource:null,
    //       userEmail:'',
    //       userPassword:'',
    //       name:''

    //     }
        
    //   }
    
    //   signIn(){
        
    //     this.setState({
    //       isLoading:true,
    //     })
    //     const response = fetch(
    //       "https://qa.openpixs.com/opSignup", {
    //           method: 'POST',
    //           headers: {
    //           Accept: 'application/json',
    //               'Content-Type': 'application/json',
    //         },
    //         body: JSON.stringify({
    //           email: this.state.userEmail,
    //           password : this.state.userPassword,
    //           name : this.state.userName,
    //           isBrand : '0',
    //           isMobileUser: 'true'
    //         }),
    //         }) 
    //         .then((response) => { return  response.json() } ) 
    //         .catch((error) => console.warn("fetch error:", error))
    //         .then((response) => {
    //           console.log(JSON.stringify(response));
    //           if(response.status==400||response.status==500){
    //             Alert.alert(JSON.stringify(response.errormsg));
    //           }
    //           else{
    //             this.setState({
    //               isLoading:false
    //             })
    //             this.props.navigation.navigate('Eighth',{userEmail:this.state.userEmail,});
    //             Toast.show("OTP Sent!")

    //           }
    //         })
    //   }
    

    //   render() {
    //     const {navigate}=this.props.navigation;
        
    //     if(this.state.isLoading){
    //       return(
    //         <View style={{flex:1,padding:8}}>
    //          <ActivityIndicator/>
    //         </View>
    //       )
    //     }
    //     else{
    //     return(
    //       <View style={styles.containerLogin}>
            
    //         <View style={styles.containerForm}>
    //           <Image source={{uri:'https://i.imgur.com/OgZY6Cw.png'}} style={{width:250,height:150}}></Image>
    //           <View style={{flexDirection:'row',borderBottomColor:'#6423a2',
    //           borderBottomWidth:2,margin:16}}>
    //             <Icon style={{alignSelf:'center'}} 
    //             name='account-box' color='#6423a2' size={30}></Icon>
    //             <TextInput style={styles.inputBox} 
    //                 placeholder="Name"
    //                 placeholderTextColor = 'grey'
    //                 selectionColor="#6423a2"
    //                 keyboardType="default"
    //                 onChangeText={(text)=>this.setState({userName:text})}
    //                 onSubmitEditing={()=> this.password.focus()}
    //                 />
                    
    //           </View>  
    //           <View style={{flexDirection:'row',borderBottomColor:'#6423a2',
    //           borderBottomWidth:2,margin:16}}>
    //             <Icon style={{alignSelf:'center'}} 
    //             name='email' color='#6423a2' size={30}></Icon>
    //             <TextInput style={styles.inputBox} 
    //                 placeholder="Email"
    //                 placeholderTextColor = 'grey'
    //                 selectionColor="#6423a2"
    //                 keyboardType="email-address"
    //                 onChangeText={(text)=>this.setState({userEmail:text})}
    //                 onSubmitEditing={()=> this.password.focus()}
    //                 />
    //           </View>  
    //           <View style={{flexDirection:'row',borderBottomColor:'#6423a2',borderBottomWidth:2,margin:16}}>
    //             <Icon style={{alignSelf:'center'}} 
    //             name='lock' color='#6423a2' size={30}></Icon>  
    //             <TextInput style={styles.inputBox}  
    //                 placeholder="Password"
    //                 secureTextEntry={true}
    //                 placeholderTextColor = "grey"
    //                 onChangeText={(text)=>this.setState({userPassword:text})}
    //                 ref={(input) => this.password = input}
    //                 />
    //           </View> 
    //           <TouchableOpacity style={styles.button} onPress={()=>this.signIn()}>
    //             <Text style={styles.buttonText}>Signup</Text>
    //           </TouchableOpacity>     
    //         </View>
    //         <View style={styles.signupTextCont}>
    //           <Text style={styles.signupText}>Already Have Account?</Text>
    //           <TouchableOpacity onPress={this.signup} onPress={()=>navigate('Fourth')}
    //           style={styles.buttonSignUp}>
    //           <Text style={styles.signupButton}> Login</Text>
    //           </TouchableOpacity>
    //         </View>
    //       </View>	
    //       )
    //   }
    // }
    // }


  //  class SearchUser extends Component {
  //     constructor() {
  //       super();
  //       this.state = {
  //         dataSource: [],
  //         isLoading:true,
  //         searchText:"",
  //         searchType:'user',
  //         userGuid:'',
  //         isEmpty:true,
  //         isFirst:true
  //       };

  //       this.arrayHolder=[];
  //     }
  //   componentDidMount() {
  //       BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);

  //       this._fetchData();
  //     }

  //   componentWillUnmount(){
  //       BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
  //     }
  
  //   handleBackButton=()=>{
  //       this.props.navigation.navigate('Sixth');
  //   }

  //     _fetchData(){
  //       this.setState({
  //         isLoading:true,
  //       })
        
  //       const response = fetch("https://qa.openpixs.com/opSearch?cursor=initial&dboffset=0&limit=200&term="+this.state.searchText+"&type="+this.state.searchType, {
  //               method: 'GET',
  //               headers: {
  //               Accept: 'application/json',
  //                   'Content-Type': 'application/json',
                    
  //             },
  //             }) 
  //             .then((response) => { return  response.json() } ) 
  //             .catch((error) => console.warn("fetch error:", error))
  //             .then((response) => {
  //               console.log(JSON.stringify(response));
  //               if(response.data==null){
  //                 this.setState({
  //                   isEmpty:true
  //                 })
  //               }
  //               else{
  //               this.setState({
  //                 dataSource:response.data,
  //                 isLoading:false,
  //                 isEmpty:false,
  //               });
  //               this.arrayHolder=response.data;
  //             }
  //             })
              
  //     }
  //     // searchFilterFun=text=>{
  //     //       if(this.state.searchType=='user'){
  //     //       const newData=this.arrayHolder.filter(item=>{
  //     //       const itemData=`${item.name.toUpperCase()}`;
  //     //       const textData=text.toUpperCase();
  //     //       return itemData.indexOf(textData)>-1;
  //     //     });
  //     //     this.setState({dataSource:newData,searchText:text})
  //     //   }

  //     //     if(this.state.searchType=='label'){
  //     //       const newData=this.arrayHolder.filter(item=>{
  //     //       const itemData=`${item.tags.toUpperCase()}`;
  //     //       const textData=text.toUpperCase();
  //     //       return itemData.indexOf(textData)>-1;
  //     //     });
  //     //     this.setState({dataSource:newData,searchText:text})
  //     //   }

  //     //     if(this.state.searchType=='location'){
  //     //       const newData=this.arrayHolder.filter(item=>{
  //     //       const itemData=`${item.location.toUpperCase()}`;
  //     //       const textData=text.toUpperCase();
  //     //       return itemData.indexOf(textData)>-1;
  //     //     });
  //     //     this.setState({dataSource:newData,searchText:text})
  //     //   }

          
  //     // }

  //     updateSearch=(searchType)=>{
  //       this.setState({
  //         searchType:searchType,
  //         dataSource:[],
  //       })

  //       this.arrayHolder=[];
        
  //     }

  //     // renderHeader=()=>{
  //     //   const {state}=this.props.navigation;
  //     //   let search=this.state.searchText;
  //     //   if(state.params==null){
          
  //     //   }
  //     //   else{
  //     //     if(state.params.isRefreshing){
  //     //      this.searchFilterFun(state.params.search)
  //     //      state.params.isRefreshing=false;
  //     //     }
  //     //   }
      
  //     //   return(
  //     //     <View>
  //     //     <SearchBar
  //     //         placeholder="Search Here..."
  //     //         inputStyle={{backgroundColor:'white',height:30,borderColor:'white'}}
  //     //         containerStyle={{backgroundColor:'white',borderColor:'white'}}
  //     //         inputContainerStyle={{backgroundColor:'white',borderColor:'white'}}
  //     //         onChangeText={searchText=>this.searchFilterFun(searchText)}
  //     //         value={search}
  //     //         autoCorrect={false}
  //     //       >
  //     //       </SearchBar>
  //     //       </View>
            
  //     //   );
  //     // }

  //     // renderSeperator=()=>{
  //     //   return(
  //     //     <View
  //     //       style={{
  //     //         height:1,
  //     //         width:'86%',
  //     //         backgroundColor:'#fff',
  //     //         marginLeft:'14%',
  //     //       }}
  //     //     >

  //     //     </View>
  //     //   );
  //     // }
  //     render() {
  //       const {state}=this.props.navigation;
  //       if(state.params==null){
          
  //       }
  //       else{
  //         if(state.params.isRefreshing){
  //          this.setState({
  //            searchText:state.params.search
  //          })
  //          Toast.show(state.params.searchText)
  //          this._fetchData();
  //          state.params.isRefreshing=false;
  //         }
  //       }

  //       if(!this.state.isFirst){
  //       if(this.state.isLoading){
  //         return(
  //           <View style={{flex:1,padding:8}}>
  //            <ActivityIndicator/>
  //           </View>
  //         )
  //       }
  //       this.setState({
  //         isFirst:false
  //       })
  //     }
  //       else{
  //       return (
  //         <View style={{justifyContent: 'center',flex: 1,}}>
            
  //         <ScrollView>
  //           <View style={{flexDirection:'row'}}>
  //           <View style={{width:'30%',height:45,borderWidth:1,borderColor:'#EAEAEA'}}>
  //             <Picker selectedValue={this.state.searchType} onValueChange={this.updateSearch}>
  //               <Picker.Item label="Users" value="user"/>
  //               <Picker.Item label="Label" value="label"/>
  //               <Picker.Item label="Location" value="location"/>
  //             </Picker>
  //           </View>
  //             <TextInput style={{width:'70%',height:45,paddingHorizontal:16,borderWidth:1,
  //                                 borderColor:'#EAEAEA'}} 
  //                 placeholder="Enter Search here..."
  //                 onChangeText={searchText=>this.setState({searchText:searchText})}
  //                 placeholderTextColor = '#6423a2'
  //                 selectionColor="#6423a2"
  //                 keyboardType="default"
  //                 />   
  //           </View>
  //           <View style={{width:'100%',flexDirection:'row',justifyContent:'center'}}>
  //           <Button style={{backgroundColor:'#6423a2',width:'90%',height:45,justifyContent:'center',
  //           marginTop:4,alignSelf:'center',elevation:10}} onPress={()=>this._fetchData()}>
  //             <Text style={{alignSelf:'center',color:'#fff',fontWeight:'bold'}}>SEARCH</Text> 
  //           </Button>
  //           </View>
  //           <View>
  //             {this.state.isEmpty?
  //             <Text style={{alignSelf:'center'}}>No Item Found for a Search!!</Text>
  //             :
  //             null
  //             }
  //           </View>
  //           {this.state.searchType=='user'?
  //           <FlatList
  //             data={this.state.dataSource}
  //             renderItem={({ item }) => (
  //               <View style={{ flex: 1, flexDirection: 'column', margin: 1,width:100,height:100,alignItems:'center'}}>
  //                 <TouchableOpacity onPress={()=>this.props.navigation.navigate('Eleventh',{userID:item.guid,isRefreshing:true})}>
  //                  {item.profile_image_url==null?
  //                  <Icon style={styles.imageThumbnail} name="account-circle" size={50} />
  //                 :

  //                 <Image style={styles.imageThumbnail} source={{ uri: item.profile_image_url }} />
  //                 }
  //                 </TouchableOpacity>
  //               <Text style={{textAlign:'center',width:100,marginBottom:20,}}>{item.name}</Text>
  //               </View>
  //             )}
  //             //Setting the number of column
  //             numColumns={4}
  //             keyExtractor={(item) => item.profile_image_url}
  //             ItemSeparatorComponent={this.renderSeperator}
  //             ListHeaderComponent={this.renderHeader}
  //           />
  //           :
  //           <View style={{height:0}}></View>
  //           }
  //           {
  //             this.state.searchType=='label'?
  //           <FlatList
  //             data={this.state.dataSource}
  //             renderItem={({ item }) => (
  //               <View style={{ flex: 1, flexDirection: 'column', margin: 1,width:100,height:100,alignItems:'center'}}>
  //                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('Tenth',
  //                       {imgSrc:item.imgguid,isRefreshing:true})}>
  //                   <Image style={{height:100,width:100,margin:4,backgroundColor:'grey'}} source={{ uri:
  //                   'https://d1n5ldzwzxqojp.cloudfront.net/'+item.userguid+'/'+item.path}} />
  //                   </TouchableOpacity>
  //               </View>
  //             )}
  //             //Setting the number of column
  //             numColumns={3}
  //             keyExtractor={(item) => item.path}
  //             ItemSeparatorComponent={this.renderSeperator}
  //             ListHeaderComponent={this.renderHeader}
  //           />
  //           :
  //           <View style={{height:0}}></View>
  //           }
  //           {
  //             this.state.searchType=='location'?
  //           <FlatList
  //             data={this.state.dataSource}
  //             renderItem={({ item }) => (
  //               <View style={{ flex: 1, flexDirection: 'column', margin: 1,width:100,height:100,alignItems:'center'}}>
  //                 <TouchableOpacity onPress={()=>this.props.navigation.navigate('Tenth',
  //                       {imgSrc:item.imgguid,isRefreshing:true})}>                  
  //                   <Image style={{height:100,width:100,margin:4}} source={{ uri:
  //                   'https://d1n5ldzwzxqojp.cloudfront.net/'+item.userguid+'/'+item.path}} />
  //                 </TouchableOpacity>
  //               </View>
  //             )}
  //             //Setting the number of column
  //             numColumns={3}
  //             keyExtractor={(item) => item.profile_image_url}
  //             // ItemSeparatorComponent={this.renderSeperator}
  //             // ListHeaderComponent={this.renderHeader}
  //           />
  //           :
  //           <View style={{height:0}}></View>
  //           }
  //           </ScrollView>
  //         </View>
  //       );
  //       }
  //     }
  //   }

    // class OtpPage extends React.Component {

    //   constructor() {
    //     super();
    //     this.state = {
    //       userEmail:'',
    //       otpVal:0,
    //     };
    //   }

    //   componentDidMount(){
    //     const {state}=this.props.navigation; 
    //     this.setState({
    //       userEmail:state.params.userEmail,
    //     })
    //   }
    
    //   verifyOTP(otpVal){
    //     Toast.show(otpVal+this.state.userEmail)
    //     const response = fetch(
    //       "https://qa.openpixs.com/opValidateOTP", {
    //           method: 'POST',
    //           headers: {
    //           Accept: 'application/json',
    //               'Content-Type': 'application/json',
                  
    //         },
    //         body: JSON.stringify({
    //           email : this.state.userEmail,
    //           otp : otpVal,
    //         }),
    //         }) 
    //         .then((response) => { return  response.json() } ) 
    //         .catch((error) => console.warn("fetch error:", error))
    //         .then((response) => {
    //           console.log(JSON.stringify(response));
    //             Toast.show(JSON.stringify(response),Toast.SHORT);
    //             this.props.navigation.navigate('Fourth')
    //         })

    //   }
    //   render() {

    //     const {navigate}=this.props.navigation;

    //     return(
    //       <View style={{height:400,flexDirection:'column'}}>
            
    //         <View style={styles.containerForm}>
    //             <Text style={{color:"#6423a2",fontSize:20}}>One Time Password(OTP)</Text>
    //             <View style={{borderBottomColor:'#6423a2',borderBottomWidth:2}}>
    //             <TextInput style={{width:150,backgroundColor:'rgba(255, 255,255,0.2)',
    //               borderRadius: 25,paddingHorizontal:16,fontSize:16,color:'grey',}}
    //                 selectionColor="#6423a2"
    //                 keyboardType="numeric"
    //                 onChangeText={(text)=>this.setState({otpVal:text})}
    //                 onSubmitEditing={()=> this.password.focus()}
    //                 /> 
    //              </View>     
    //           <TouchableOpacity style={styles.buttonVerify} onPress={()=>this.verifyOTP(
    //             this.state.otpVal)}>
    //             <Text style={styles.buttonText}>Verify</Text>
    //           </TouchableOpacity>     
    //         </View>
    //       </View>	
    //       )
    //   }
    // }

    // class PrivacyFaqPage extends React.Component{

    //   constructor() {
    //     super();
    //     this.state = {
    //       webValue:''
    //     };
    //   }

    //   render() {
          
    //       const {state}=this.props.navigation;
    //       if(state.params.isRefreshing){
    //       if(state.params.id=='privacy'){
    //         this.setState({
    //           webValue:'https://www.openpixs.com/privacy.html'
    //         })
    //       }else{
    //         this.setState({
    //           webValue:'https://www.openpixs.com/privacy.html'
    //         })
    //       }
    //       state.params.isRefreshing=false;
    //     }
    //     return(
    //       <View style={{height:500,flexDirection:'column'}}>
    //       {Toast.show(this.state.webValue)}
    //         <WebView 
    //         source={{uri:this.state.webValue}}
    //         style={{margin:4}}
    //         >
    //         </WebView>
    //       </View>	
    //       )
    //   }
    // }

    // class NotificationPage extends React.Component {

    //   constructor(props) {
 
    //     super(props);
    //     this.state={
    //       isLoading:true,
    //       dataList:[],
    //       userId:''
    //     }
      
    //     YellowBox.ignoreWarnings([
    //      'Warning: componentWillMount is deprecated',
    //      'Warning: componentWillReceiveProps is deprecated',
    //    ]);
      
    //   }
     
    // componentDidMount(){
    //     BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);

    //     this._fetchData();
    //   }

    // componentWillUnmount(){
    //     BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    //   }
  
    // handleBackButton=()=>{
    //     this.props.navigation.navigate('Sixth');
    // }

    //   _fetchData(){
    //     AsyncStorage.getItem('userToken',(err,result)=>{
    //       AsyncStorage.getItem('userGuid',(err,userId)=>{      
    //       const response = fetch(
    //         "https://qa.openpixs.com/opGetNotifications", {
    //             method: 'GET',
    //             headers: {
    //             Accept: 'application/json',
    //                 'Content-Type': 'application/json',
    //                 'Authentication' : result,
    //                 // change authentication
    //           },
    //           }) 
    //           .then((response) => { return  response.json() } ) 
    //           .catch((error) => console.warn("fetch error:", error))
    //           .then((response) => {
    //             console.log(JSON.stringify(response));
    //             this.setState({
    //               dataList:response,
    //               isLoading:false,
    //               userId:userId
    //             });
    //           })
    //         });
    //       });
    //   }

    //   _updateView(id){
    //     this._fetchData();

    //     AsyncStorage.getItem('userToken',(err,resultToken)=>{
       
    //       const response = fetch(
    //         "https://qa.openpixs.com/opMarkNotificationRead?status=1&id="+id, {
    //             method: 'PUT',
    //             headers: {
    //             Accept: 'application/json',
    //                 'Content-Type': 'application/json',
    //                 'Authentication' : resultToken,
    //           },
    //           }) 
    //           .then((response) => { return  response.json() } ) 
    //           .catch((error) => console.warn("fetch error:", error))
    //           .then((response) => {
    //             console.log(JSON.stringify(response));
    //             Toast.show(JSON.stringify(response),Toast.SHORT);
    //           });
    //         });
    //   }

    //   _viewAll(){
    //     this._fetchData();
    //     AsyncStorage.getItem('userToken',(err,resultToken)=>{
       
    //       const response = fetch(
    //         "https://qa.openpixs.com/opMarkNotificationRead?status=1&all=1", {
    //             method: 'PUT',
    //             headers: {
    //             Accept: 'application/json',
    //                 'Content-Type': 'application/json',
    //                 'Authentication' : resultToken,
    //           },
    //           }) 
    //           .then((response) => { return  response.json() } ) 
    //           .catch((error) => console.warn("fetch error:", error))
    //           .then((response) => {
    //             console.log(JSON.stringify(response));
    //             Toast.show(JSON.stringify(response),Toast.SHORT);
    //           });
    //         });
    //   }

    //   render() {

    //     const {navigate}=this.props.navigation;
    //     if(this.state.isLoading){
    //       return(
    //         <View style={{flex:1,padding:8}}>
    //          <ActivityIndicator/>
    //         </View>
    //       )
    //     }
    //     else{
    //     return(
    //       <View>
    //         <View style={{alignItems:'center',flexDirection:'row',width:'100%',
    //               justifyContent:'space-around',marginTop:16}}>
    //             <TouchableOpacity onPress={()=>navigate('Sixth')}>
    //               <Icon style={{alignSelf:'center'}} 
    //                 name='arrow-back' color='#6423a2' size={30}></Icon>
    //             </TouchableOpacity>      
    //               <Text style={{color:"#6423a2",fontSize:20}}>NOTIFICATIONS</Text>
    //               <TouchableOpacity onPress={()=>this._viewAll()}>
    //               <Icon style={{alignSelf:'center'}} name='cancel' color='#6423a2' size={50}></Icon>
    //               </TouchableOpacity>
    //             </View>
    //         <FlatList
    //         data={this.state.dataList}
    //         renderItem={({item})=>
    //           <View style={{flexDirection:'column'}}>

    //             {item.type=='like'?
    //             <View style={{height:250,marginTop:4}}>
    //               <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Tenth',
    //                     {imgSrc:item.imgGuid,isRefreshing:true}),this._updateView(item.id)}}>  
    //               <View style={{alignItems:'center',flexDirection:'row',width:'100%',
    //                     marginTop:16,marginLeft:16}}>
    //                 <Image style={{alignSelf:'center',height:30,width:30,borderRadius:15}} source={{uri:item.profileImg}} 
    //                     ></Image>
    //                 <Text style={{color:"#6423a2",fontSize:14,marginLeft:8,fontWeight:'bold'}}>{item.name+" Like your Photo"}</Text>
    //               </View>
    //               <View style={{flexDirection:'row-reverse',marginHorizontal:16}}>
    //               <Text>16s</Text>
    //               </View>
    //               <View style={{height:150,backgroundColor:'#EAEAEA',margin:16}}>
    //                 <Image style={{height:120,backgroundColor:'#fff',margin:16}} 
    //                 source={{uri:'https://d1n5ldzwzxqojp.cloudfront.net/'+this.state.userId+"/"+item.path}}></Image>
    //               </View>
    //               <View style={{width:'100%',backgroundColor:'#EAEAEA',height:1,marginTop:16}}></View>
    //               </TouchableOpacity>
    //             </View>
    //             :
    //               <View style={{height:50}}>
    //                 <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Eleventh',{userID:item.userGuid,isRefreshing:true}),this._updateView(item.id)}}> 
    //                 <View style={{alignItems:'center',flexDirection:'row',width:'100%',
    //                       marginTop:16,marginLeft:16}}>
    //                   <Image style={{alignSelf:'center',height:30,width:30,borderRadius:15}} source={{uri:item.profileImg}} 
    //                   ></Image>
    //                   <Text style={{color:"#6423a2",fontSize:14,marginLeft:8,fontWeight:'bold'}}>{item.name+" Followed You"}</Text>
    //                 </View>
    //                 <View style={{width:'100%',backgroundColor:'#EAEAEA',height:1,marginTop:16}}></View>           
    //                 </TouchableOpacity> 
    //             </View>
    //          }
    //          </View>
    //         }
    //         keyExtractor={(item,index)=>'list-item-${index}'}>
    //       </FlatList>	
    //     </View>
    //     )
    //     }
    //   }
    // }

    
//     class FollowersList extends React.Component {
//       constructor(props) {
 
//         super(props);
//         this.state={
//           isLoading:true,
//           dataList:[],
//           userId:'',
//           myUserId:''
//         }
      
//         YellowBox.ignoreWarnings([
//          'Warning: componentWillMount is deprecated',
//          'Warning: componentWillReceiveProps is deprecated',
//        ]);
      
//       }
     
//       componentDidMount(){
//         BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
//        // this.interval=setInterval(()=>this._fetchData,this.setState({time:Date.now()}),10000)  
//     }
    
//     componentWillUnmount(){
//         BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
//       }

//     handleBackButton=()=>{
//         this.props.navigation.goBack();
//     }

//       _fetchFollowers(){
//         const {state}=this.props.navigation;
//         AsyncStorage.getItem('userToken',(err,result)=>{
//           AsyncStorage.getItem('userGuid',(err,userGuid)=>{      
//           const response = fetch(
//             "https://qa.openpixs.com​/opUserFollowerList?id="+state.params.userId+"&limit=100&offset=0", {
//                 method: 'GET',
//                 headers: {
//                 Accept: 'application/json',
//                     'Content-Type': 'application/json',
//                     'Authentication' : result,
//                     // change authentication
//               },
//               }) 
//               .then((response) => { return  response.json() } ) 
//               .catch((error) => console.warn("fetch error:", error))
//               .then((response) => {
//                 console.log(JSON.stringify(response));
//                 this.setState({
//                   dataList:response,
//                   isLoading:false,
//                   myUserId:userGuid,
//                 });
//               })
//           });
//         });
//       }

//       _fetchFollowing(){
//         const {state}=this.props.navigation;
//         AsyncStorage.getItem('userToken',(err,result)=>{
//           AsyncStorage.getItem('userGuid',(err,userGuid)=>{          
//           const response = fetch(
//             "https://qa.openpixs.com​/opUserFollowingList?id="+state.params.userId+"&limit=100&offset=0", {
//                 method: 'GET',
//                 headers: {
//                 Accept: 'application/json',
//                     'Content-Type': 'application/json',
//                     'Authentication' : result,
//                     // change authentication
//               },
//               }) 
//               .then((response) => { return  response.json() } ) 
//               .catch((error) => console.warn("fetch error:", error))
//               .then((response) => {
//                 console.log(JSON.stringify(response));
//                 this.setState({
//                   dataList:response,
//                   isLoading:false,
//                   myUserId:userGuid,
//                 });
//               })
//           });
//         });
//       }

//       render() {
//         const {state}=this.props.navigation; 
//         const {navigate}=this.props.navigation;
//         if(state.params.isFollow==1){
//           this._fetchFollowers();
//         }
//         else{
//           this._fetchFollowing();
//         }
//         return(
//           <View>
//             <FlatList
//             data={this.state.dataList}
//             renderItem={({item})=>
//               <View>
//                 <View style={{flexDirection:'row',margin:4,alignContent:'center',
//                 justifyContent:'center',alignItems:'center'}}>
//                 <TouchableOpacity onPress={()=>this.props.navigation.navigate('Eleventh',{userID:item.id,isRefreshing:true})}>
//                 {item.profileImage==null?
//                   <Icon style={{marginRight:16,marginTop:4}} name="account-circle" size={45} color="#6423a2"></Icon>
//                   :
//                   <Image style={{width:40,height:40,borderRadius:20,marginRight:16,marginTop:4}} source={{uri:item.profileImage}}></Image>
//                 }
//                 </TouchableOpacity>
//                   <Text style={{width:'60%',fontSize:16,fontWeight:'bold'}}>{item.name}</Text>
//                   {item.id==this.state.myUserId?
//                   <View style={{width:'20%'}}>
//                   </View>
//                   :
//                   <View style={{width:'20%'}}>
//                   {item.isfollowed==0?
//                   <Text>Non Following</Text>
//                   :
//                   <Text>Following</Text>
//                   }
//                   </View>
//                 }
//                 </View>
//                 <View style={{height:1,backgroundColor:'#EAEAEA',marginTop:4}}></View>
//               </View>

//             } 
//             keyExtractor={(item,index)=>'list-item-${index}'}>
//           </FlatList>	
//           </View>
//         )
//     }
// }

    // class ViewImage extends React.Component {

    //   constructor() {
    //     super();
    //     this.state = {
    //       isSecond:false,
    //       dataList:{},
    //     };
    //   }

    //   _fetchData(imgId){
    //     AsyncStorage.getItem('userToken',(err,result)=>{
    //       const response = fetch(
    //         "https://qa.openpixs.com/opGetImageDetails?imgGuid="+imgId, {
    //             method: 'GET',
    //             headers: {
    //             Accept: 'application/json',
    //                 'Content-Type': 'application/json',
    //                 'Authentication' : result,
                    
    //           },
    //           }) 
    //           .then((response) => { return  response.json() } ) 
    //           .catch((error) => console.warn("fetch error:", error))
    //           .then((response) => {
    //             console.log(JSON.stringify(response));
    //             this.setState({
    //               dataList:response,
    //               isSecond:true,
    //             });
    //           })
    //         });
    //   }

    //   _onLikeImage(imageId,action){
    //     AsyncStorage.getItem('userToken',(err,resultToken)=>{
           
    //       const response = fetch(
    //         "https://qa.openpixs.com/opImageLikeUnlike", {
    //             method: 'POST',
    //             headers: {
    //             Accept: 'application/json',
    //                 'Content-Type': 'application/json',
    //                 'Authentication' : resultToken,
    //           },
    //           body: JSON.stringify({
    //             imgGuid:imageId,
    //             action:action,
    //           }),
    //           }) 
    //           .then((response) => { return  response.json() } ) 
    //           .catch((error) => console.warn("fetch error:", error))
    //           .then((response) => {
    //             console.log(JSON.stringify(response));
    //             Toast.show(JSON.stringify(response)+action);
    //             this._fetchData(imageId);
    //           });
    //         });
    //   }

    //   handleBackButton=()=>{
    //     this.props.navigation.navigate('Tenth')
    //     return true;
    //   }

    //     render() {
    //       const {state}=this.props.navigation;
    //       if(state.params.isRefreshing){
    //         this._fetchData(state.params.imgSrc);
    //         state.params.isRefreshing=false;
            
    //       } 
    //       const {navigate}=this.props.navigation;
    //         return(
    //           <View style={{flex:1,flexDirection:'column'}}>
    //             <ScrollView>
    //             <View style={{height:55, flexDirection:'row',padding:8}}>
    //             {/* <TouchableOpacity onPress={()=>this.props.navigation.goBack(state.params.goBackKey)}>
    //                 <Icon style={{alignSelf:'center'}} name="arrow-back" size={30}></Icon>
    //               </TouchableOpacity> */}
    //               <View style={{flexDirection:'row',justifyContent:'center',width:'100%'}}>
    //               <TouchableOpacity style={{alignSelf:'center'}} onPress={()=>navigate('Sixth') }>
    //                 <Image source={{uri:'https://i.imgur.com/OgZY6Cw.png',width:150,height:40}}></Image>
    //               </TouchableOpacity> 
    //               </View> 
    //             </View>
    //             <View>
    //             <Image style={{height:350,margin:4}}
    //              source={{uri:"https://d1n5ldzwzxqojp.cloudfront.net/"+this.state.dataList.userguid+"/"+this.state.dataList.path}}>
    //              </Image>
    //             <View style={{padding:16,flexDirection:'row',justifyContent:'space-between',width:'70%',alignSelf:'center'}}>
    //               <View style={{justifyContent:'center'}}>
    //               <Image style={{width:30,height:30,alignSelf:'center'}} source={{uri:'https://i.imgur.com/5TTsKOj.png'}}></Image>
    //               <Text>{this.state.dataList.imageViewCount+" Views"}</Text>
    //               </View>
    //               <View>
    //               {this.state.dataList.isLiked==0?
    //               <TouchableOpacity onPress={()=>this._onLikeImage(state.params.imgSrc,"1")}>
    //                 <Image  style={{width:25,height:25}} source={{uri:"https://i.imgur.com/3Ak4Qch.png"}}></Image>
    //               </TouchableOpacity>
    //                 :
    //               <TouchableOpacity onPress={()=>this._onLikeImage(state.params.imgSrc,"0")}>
    //                 <Image style={{width:25,height:25}} source={{uri:"https://i.imgur.com/4ermCMf.png"}}></Image>
    //               </TouchableOpacity>
    //               }
    //               <Text>{this.state.dataList.likescount+" Likes"}</Text>
    //               </View>
    //             </View>
    //             <Text style={{fontSize:18,marginLeft:4,fontWeight:'bold',}}>DESCRIPTION:</Text>
    //             <Text style={{margin:8}}>{this.state.dataList.description}</Text>
    //             <Text style={{fontSize:18,marginLeft:4,fontWeight:'bold'}}>TAGS:</Text>
    //             <Text style={{margin:8}}>{this.state.dataList.tags}</Text>
    //             <Text style={{fontSize:18,marginLeft:4,fontWeight:'bold'}}>LOCATION:</Text>
    //             <Text style={{margin:8}}>{this.state.dataList.location}</Text>
    //           </View>
    //           </ScrollView>
    //           </View>
    //         )
          
    //     }
    //   }
    
    

    const SecondActivity_StackNavigator = createStackNavigator({
      Second: { 
        screen: SecondActivity, 
        navigationOptions: ({ navigation }) => ({
          
          headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });


    const ThirdActivity_StackNavigator = createStackNavigator({
      Third: { 
        screen: ThirdActivity, 
        navigationOptions: ({ navigation }) => ({
          
          headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    

    const FourthActivity_StackNavigator = createStackNavigator({
      Fourth: { 
        screen: Login,
        navigationOptions: {
          drawerLabel:()=>null,
          header:null,
          headerStyle:{backgroundColor:'#fff'}
          
        }
      },
    });

    const FifthActivity_StackNavigator = createStackNavigator({
      Fifth: { 
        screen: Signup, 
        navigationOptions: {
          drawerLabel:()=>null,
          header:null,
          headerStyle:{backgroundColor:'#fff'}
          
        }
      },
    });

    const SixthActivity_StackNavigator = createStackNavigator({
      Sixth: { 
        screen: MainActivity, 
        navigationOptions: ({ navigation }) => ({
          
          headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    const SeventhActivity_StackNavigator = createStackNavigator({
      Seventh: { 
        screen: SearchUser, 
        navigationOptions: ({ navigation }) => ({
          
          headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    const EighthActivity_StackNavigator = createStackNavigator({
      Eighth: { 
        screen: OtpPage, 
        navigationOptions: ({ navigation }) => ({
          header:null,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    const NinthActivity_StackNavigator = createStackNavigator({
      Ninth: { 
        screen: NotificationPage, 
        navigationOptions: ({ navigation }) => ({
          header:null,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    const TenthActivity_StackNavigator = createStackNavigator({
      Tenth: { 
        screen: ViewImage, 
        navigationOptions: ({ navigation }) => ({
          header:null,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    const EleventhActivity_StackNavigator = createStackNavigator({
      Eleventh: { 
        screen: UserDashboardActivity, 
        navigationOptions: ({ navigation }) => ({
          headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    const TwelvethActivity_StackNavigator = createStackNavigator({
      Twelveth: { 
        screen: FollowersList, 
        navigationOptions: ({ navigation }) => ({
          headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    const ThirteenActivity_StackNavigator = createStackNavigator({
      Thirteen: { 
        screen: PrivacyFaqPage, 
        navigationOptions: ({ navigation }) => ({
          headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    const FourteenActivity_StackNavigator = createStackNavigator({
      Fourteen: { 
        screen: TagsPage, 
        navigationOptions: ({ navigation }) => ({
          header:null,

          headerStyle: {
            backgroundColor: '#fff'
          },
          headerTintColor: '#fff',
        })
      },
    });

    //Drawer Menu
 class Custom_side_menu extends Component{
   constructor(props){
     super(props);
     this.state={
       userName:null,
       profileUrl:null,
       isFirst:true,
     }
   }

   _fetchData(){
    AsyncStorage.getItem('userGuid',(err,resultID)=>{
      AsyncStorage.getItem('userToken',(err,resultToken)=>{
        const response = fetch(
          "https://qa.openpixs.com/opUserDashboardStats?id="+resultID, {
              method: 'GET',
              headers: {
              Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authentication' : resultToken,
            },
            }) 
            .then((response) => { return  response.json() } ) 
            .catch((error) => console.warn("fetch error:", error))
            .then((response) => {
              console.log(JSON.stringify(response));
              this.setState({
                userName:response.name,
                profileUrl:response.profileImageUrl,
                isFirst:false,
              });
            })
      });
    });
   }

   removeBack(){
    BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
}

   handleBackButton=()=>{
    this.props.navigation.navigate('Sixth');
    BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
    return true;
  
  }
  updateUILogin(){
    BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
    
    AsyncStorage.setItem('userName',"");
    AsyncStorage.setItem('userEmail',"");
    AsyncStorage.setItem('userPassword',"");

    AsyncStorage.getItem('userEmail',(err,userEmail)=>{
    Toast.show("Successfully Logout");
    });
  }

   render(){
     if(this.state.isFirst){
       this._fetchData();
     }else{
     AsyncStorage.getItem('userEmail',(err,resultToken)=>{
        if(resultToken==null){
          this._fetchData();
        }
     });
    }
     return(
      
        <View>
          <View style={{height:150,backgroundColor:"#6423a2",flexDirection:'column-reverse'}}>
            <View style={{flexDirection:'row'}}>
            {this.state.profileUrl==null?
              <Icon style={{margin :16,}} size={50} name='account-circle' color='white'></Icon>
              :
              <Image style={{margin :16, height:50,width:50,borderRadius:25}} source={{uri:
                this.state.profileUrl}}></Image>
            }
              <Text style={{fontWeight:'bold',alignSelf:'center',color:'white',fontSize:16}}>
              {this.state.userName}</Text>
            
            </View>
          </View>
          <View style={{width:'100%',padding:8,margin:16}}>
          <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Sixth'),this.removeBack()}}>
          <View style={{flexDirection:'row',alignItems:'center',marginTop:16}}>
            <Icon style={{marginRight :8}} name='home' color='#6423a2' size={30}></Icon>
            <Text style={{fontSize:12,fontWeight:'bold'}}>Home</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Third'),this.removeBack()}}>
          <View style={{flexDirection:'row',alignItems:'center',marginTop:16}}>
            <Icon style={{marginRight :8}} name='settings' color='#6423a2' size={30}></Icon>
            <Text style={{fontSize:12,fontWeight:'bold'}} >Settings</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Second'),this.removeBack()}}>
          <View style={{flexDirection:'row',alignItems:'center',marginTop:16}}>
            <Icon style={{marginRight :8}} name='account-box' color='#6423a2' size={30}></Icon>
            <Text style={{fontSize:12,fontWeight:'bold'}} >My Profile</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{
              this.updateUILogin(),
              this.props.navigation.navigate('Fourth'),this.removeBack()}}>
          <View style={{flexDirection:'row',alignItems:'center',marginTop:16}}>
            <Icon style={{marginRight :8}} name='lock' color='#6423a2' size={30}></Icon>
            <Text style={{fontSize:12,fontWeight:'bold'}} >Logout</Text>
          </View>
          </TouchableOpacity>
          </View>
        </View>
     );
   }
 } 
 
 
 const MyDrawerNavigator = createDrawerNavigator({
  screen:FourthActivity_StackNavigator,

  Setting: {
    screen: ThirdActivity_StackNavigator
  },

  'My Profile': { 
    screen: SecondActivity_StackNavigator
  },
  Logout:{
    
    screen: FourthActivity_StackNavigator
  },

  Home:{
    screen: SixthActivity_StackNavigator
  },

  Signup:{
    screen:FifthActivity_StackNavigator,
    
  },
  Search:{
    screen:SeventhActivity_StackNavigator
  },
  Otp:{
    screen:EighthActivity_StackNavigator
  },
  Notification:{
    screen:NinthActivity_StackNavigator
  },
  ViewImage:{
    screen:TenthActivity_StackNavigator
  },
  FAQ:{
    screen:EleventhActivity_StackNavigator
  },
  FollowersList:{
    screen:TwelvethActivity_StackNavigator
  },
  PrivacyFaq:{
    screen:ThirteenActivity_StackNavigator
  },
  Tags:{
    screen:FourteenActivity_StackNavigator
  }

},{
  contentComponent:Custom_side_menu,
});

const App=createAppContainer(MyDrawerNavigator);

export default App;
    
