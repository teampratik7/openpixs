import {StyleSheet,Dimensions,Platform} from 'react-native'
const styles = StyleSheet.create({
    container: {
      width:Dimensions.get('window').width,
      flexDirection:'row',
      flex:1,
      backgroundColor:'#000',
      justifyContent:'space-around'
      
    },
    navBar:{
      height: 55,
      alignSelf:'stretch',
      backgroundColor: '#fff',
      justifyContent:'space-around',
      alignContent:'center',
      alignItems:'center',
      flexDirection:'row',
      padding: 8,
      width:100,
      flex:1
    },
    navHead:{
      elevation: 4,
      flexDirection:'row',
      backgroundColor: '#000',
      flex:1,
      width:100
    },
    MainContainer :{
   
      flex:1,
      paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
      alignItems: 'center',
      justifyContent: 'center',
      },
  
      containerLogin : {
        backgroundColor:'#fff',
        flex: 1,
        alignItems:'center',
        justifyContent :'center'
      },
      signupTextCont : {
        justifyContent :'center',
        paddingVertical:16,
        flexDirection:'column'
      },
      signupText: {
        color:'black',
        fontSize:16,
        alignSelf:'center'
      },
      signupButton: {
        color:'#fff',
        fontSize:16,
        fontWeight:'500',
        alignSelf:'center'
      },
    inputBox: {
      width:300,
      backgroundColor:'rgba(255, 255,255,0.2)',
      borderRadius: 25,
      paddingHorizontal:16,
      fontSize:16,
      color:'grey',
      
    },
  
   
    button: {
      width:300,
      backgroundColor:'#6423a2',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:16
    },
    buttonVerify: {
      width:300,
      backgroundColor:'#6423a2',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:32
    },
    buttonSignUp: {
      width:300,
      backgroundColor:'#aa8cc5',
       borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop:16
    },
    buttonText: {
      fontSize:16,
      fontWeight:'500',
      color:'#ffffff',
      textAlign:'center'
    },
    containerForm : {
      flexGrow: 1,
      justifyContent:'center',
      alignItems: 'center'
    },
    imageThumbnail: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 50,
      width:50,
      marginTop:20,
    borderRadius:25
  
    }
  });